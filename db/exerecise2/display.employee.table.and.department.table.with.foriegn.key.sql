step 1:-
CREATE TABLE `training`.`employee` (
          `employee`_id` INT NOT NULL,
          `first_name` VARCHAR(45) NOT NULL,
          `surname` VARCHAR(45) NULL,
          `dob` DATE NOT NULL,
          `date_of_joining` DATE NOT NULL,
          `annual_salary` INT NOT NULL);
step 2:-
CREATE TABLE `training`.`department` (
  `department_id` INT NOT NULL,
  `department_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`department_id`));

step 3:-
INSERT INTO `training`.`department` (`department_id`
, `department`_name`)
 VALUES ('1'
 , 'ITDesk');
INSERT INTO `training`.`department` (`department_id`
, `department_name`)
 VALUES ('2'
 , 'Finance');
INSERT INTO `training`.`department` (`department_id`
, `department_name`) 
 VALUES ('3'
 , 'Engineering');
INSERT INTO `training`.`department` (`department_id`
, `department_name`) 
 VALUES ('4'
 , 'HR');
INSERT INTO `training`.`department` (`department_id`
, `department_name`)
 VALUES ('5'
 , 'Recruitment');
INSERT INTO `training`.`department` (`department_id`
, `department_name`)
 VALUES ('6'
 , 'Facility');

step 4:-
ALTER TABLE `training`.`employee` 
ADD COLUMN `department_id` INT NULL
AFTER `annual_salary`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`department_id`),
ADD INDEX `department_id_department_idx` (`department_id` ASC) VISIBLE;
;
ALTER TABLE `training`.`employee1` 
ADD CONSTRAINT `department_id_department`
  FOREIGN KEY (`department_id`)
  REFERENCES `training`.`department` (`department_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;