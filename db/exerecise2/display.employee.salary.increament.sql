/*
-- Query:UPDATE employee 
         SET annual_salary = annual_salary+(annual_salary*.1);
   select * from employee1
LIMIT 0, 1000

-- Date: 2020-08-04 22:37
*/
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (200,'karan','','2000-01-12','2019-02-04',440000,5);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (201,'kamesh','','2000-05-05','2019-02-03',330000,NULL);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (202,'kanishka',NULL,'2000-06-01','2018-03-21',440000,5);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (203,'karthik',NULL,'2001-02-22','2019-03-01',330000,NULL);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (204,'kavipriya',NULL,'2002-03-13','2019-05-01',330000,6);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id` VALUES (205,'kiran',NULL,'2000-03-16','2018-04-01',550000,2);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (206,'kishore',NULL,'2002-07-14','2017-02-06',660000,3);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (207,'krishna',NULL,'2000-10-23','2019-04-23',440000,5);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (208,'krishnaveni',NULL,'2002-12-24','2018-02-02',330000,6);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (209,'lokesh',NULL,'2000-01-20','2019-05-05',550000,2);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (210,'nandani',NULL,'2000-01-31','2019-03-07',495000,4);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (211,'madhu sree',NULL,'2002-01-19','2018-02-05',660000,NULL);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (212,'maheswaran',NULL,'2000-03-16','2018-02-07',385000,1);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (213,'mariyappan',NULL,'2001-01-04','2018-02-04',550000,2);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (214,'mohammed','khachrod','1999-02-02','2019-05-06',660000,3);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (215,'mohan',NULL,'2001-05-12','2019-05-07',495000,4);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (216,'monisha',NULL,'2000-07-18','2019-04-06',385000,1);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (217,'mukesh',NULL,'2001-09-17','2019-05-09',550000,2);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (218,'mukhilan',NULL,'2000-01-10','2018-02-06',385000,NULL);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (219,'naleen',NULL,'2002-03-13','2018-04-01',440000,5);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (220,'nandhkumar',NULL,'2000-05-25','2019-04-05',385000,1);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (221,'naveen',NULL,'1999-02-06','2018-03-08',660000,3);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (222,'naveen',NULL,'2000-12-03','2019-09-06',385000,1);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (223,'nikhila',NULL,'2002-01-04','2018-02-03',495000,4);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (224,'nithya sree',NULL,'2001-04-09','2019-06-04',550000,2);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (225,'nivetha',NULL,'2000-07-29','2019-03-02',440000,5);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (226,'pradeep',NULL,'2002-04-30','2019-01-05',660000,3);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (227,'prasath',NULL,'2000-06-27','2019-02-03',495000,4);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (228,'pragadesh',NULL,'2001-07-30','2019-06-05',330000,6);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (229,'pavitra',NULL,'2001-01-05','2018-08-07',385000,NULL);
INSERT INTO `` (`employee_id`,`first_name`,`surname`,`dob`,`date_of_joining`,`annual_salary`,`department_id`) VALUES (230,'praveen ',NULL,'2002-01-07','2018-03-06',495000,4);
