/*
-- Query: select employee.first_name
               , employee.surname
               , department.dep_name 
           from training.employee1 employee
              , training.department department
           where employee.dep_id = department.dep_id
LIMIT 0, 1000

-- Date: 2020-08-03 21:31
*/
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('karan','','Recruitment');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('kamesh','','Facility');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('kanishka',NULL,'Recruitment');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('karthik',NULL,'Facility');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('kavipriya',NULL,'Facility');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('kiran',NULL,'Finance');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('kishore',NULL,'Engineering');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('krishna',NULL,'Recruitment');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('krishnaveni',NULL,'Facility');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('lokesh',NULL,'Finance');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('nandani',NULL,'HR');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('madhu sree',NULL,'Engineering');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('maheswaran',NULL,'ITDesk');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('mariyappan',NULL,'Finance');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('mohammed','khachrod','Engineering');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('mohan',NULL,'HR');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('monisha',NULL,'ITDesk');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('mukesh',NULL,'Finance');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('mukhilan',NULL,'ITDesk');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('naleen',NULL,'Recruitment');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('nandhkumar',NULL,'ITDesk');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('naveen',NULL,'Engineering');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('naveen',NULL,'ITDesk');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('nikhila',NULL,'HR');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('nithya sree',NULL,'Finance');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('nivetha',NULL,'Recruitment');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('pradeep',NULL,'Engineering');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('prasath',NULL,'HR');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('pragadesh',NULL,'Facility');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('pavitra',NULL,'ITDesk');
INSERT INTO `` (`first_name`,`surname`,`dep_name`) VALUES ('praveen ',NULL,'HR');
