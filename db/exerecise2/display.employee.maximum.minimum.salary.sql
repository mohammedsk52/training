/*
-- Query: SELECT employee.department_id
                ,department.department_name
                , max(employee.annual_salary) as MaxSalary 
                ,min(employee.annual_salary) as MinSalary
            FROM employee employee
               , department department 
            WHERE employee.department_id = department.department_id
            GROUP BY department_id
LIMIT 0, 1000

-- Date: 2020-08-04 23:15
*/
INSERT INTO `` (`department_id`,`department_name`,`MaxSalary`,`MinSalary`) VALUES (1,'ITDesk',385000,385000);
INSERT INTO `` (`department_id`,`department_name`,`MaxSalary`,`MinSalary`) VALUES (2,'Finance',550000,550000);
INSERT INTO `` (`department_id`,`department_name`,`MaxSalary`,`MinSalary`) VALUES (3,'Engineering',660000,660000);
INSERT INTO `` (`department_id`,`department_name`,`MaxSalary`,`MinSalary`) VALUES (4,'HR',495000,495000);
INSERT INTO `` (`department_id`,`department_name`,`MaxSalary`,`MinSalary`) VALUES (5,'Recruitment',440000,440000);
INSERT INTO `` (`department_id`,`department_name`,`MaxSalary`,`MinSalary`) VALUES (6,'Facility',330000,330000);
