/*
-- Query: USE  student;
         CREATE TABLE college(id int(4) 
                    , name varchar(25)
					, univ_code char(4)
					, city varchar(25)
					,state varchar(15)
					, year_opened year(4));
         SELECT * FROM college
         ALTER TABLE college 
           ADD PRIMARY KEY(id)
         ALTER TABLE college 
           RENAME college1
         SELECT * FROM college1;
         DROP TABLE college1;
LIMIT 0, 1000

-- Date: 2020-08-05 21:56
*/
