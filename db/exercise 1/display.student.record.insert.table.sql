/*
-- Query: SELECT * FROM college
LIMIT 0, 1000

-- Date: 2020-08-05 22:38
*/
INSERT INTO `` (`id`
               ,`name`
               ,`univ_code`
               ,`city`
               ,`state`
               ,`year_opened`
               ,`Created_on`)
 VALUES (3
        ,'KPRIET'
        ,1002
        ,'COIMBATORE'
        ,'TAMIL NADU'
        ,2009
        ,'2020-08-05');
INSERT INTO `` (`id`
               ,`name`
               ,`univ_code`
               ,`city`
               ,`state`
               ,`year_opened`
               ,`Created_on`)
 VALUES (4
        ,'ksr college'
        ,1003
        ,'COIMBATORE'
        ,'TAMIL NADU'
        ,2009
        ,'2020-08-05');
INSERT INTO `` (`id`
               ,`name`
               ,`univ_code`
               ,`city`
               ,`state`
               ,`year_opened`
               ,`Created_on`)
 VALUES (5
        ,'kmch college'
        ,1004
        ,'COIMBATORE'
        ,'TAMIL NADU'
        ,2009
        ,'2020-08-05');
INSERT INTO `` (`id`
               ,`name`
               ,`univ_code`
               ,`city`
               ,`state`
               ,`year_opened`
               ,`Created_on`)
 VALUES (6
        ,'sakti engineering college'
        ,1005
        ,'tirpur'
        ,'TAMIL NADU'
        ,2009
        ,'2020-08-05');
INSERT INTO `` (`id`
               ,`name`
               ,`univ_code`
               ,`city`
               ,`state`
               ,`year_opened`
               ,`Created_on`)
 VALUES (7
        ,'SRI ESWAR COLLEGE of TECH'
        ,1006
        ,'salem'
        ,'TAMIL NADU'
        ,2009
        ,'2020-08-05');
