USE student;
SELECT student.rollno,student.name, college.name 
 AS college_name,college.city 
 FROM student student 
 INNER JOIN college college
 ON college.id = student.college_code;
SELECT student.rollno,student.name, college.name
 AS college_name,college.city 
 FROM student student
 LEFT JOIN college college
 ON college.id = student.college_code;
SELECT student.rollno,student.name, college.name 
 AS college_name,college.city 
 FROM student student 
 RIGHT JOIN college college
 ON college.id = student.college_code;
