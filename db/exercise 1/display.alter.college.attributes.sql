/*
-- Query: ALTER TABLE college 
              ADD address varchar(25);
          SELECT * FROM college;
          ALTER TABLE college 
		      MODIFY address varchar(30);
          ALTER TABLE college 
		      RENAME COLUMN address TO addr;
          SELECT * FROM college;
          ALTER TABLE college 
              DROP COLUMN addr;
          select * FROM college;
LIMIT 0, 1000

-- Date: 2020-08-05 22:00
*/
