SELECT s.student_id 
  AS roll_number  
    ,s.student_name 
  AS student_name 
    ,s.dob  
    ,c.college_name 
  AS college_name  
    ,d.department_name
    ,sr.semester  
    ,sr.grade  
    ,sr.credits  
   FROM university u  
       ,college c  
       ,department d   
       ,designation de  
       ,college_department cd
       ,employee e   
       ,student s  
       ,syllabus sy  
       ,professor_syllabus ps  
       ,semester_result sr 
  WHERE c.university_code = u.university_code
   AND u.university_code = d.university_code 
   AND cd.college_id = c.college_id  
   AND cd.university_dept_code = d.department_code 
   AND e.college_id = c.college_id
   AND e.college_dept_id = cd.college_dept_id  
   AND e.designation_id = de.designation_id  
   AND s.college_id = c.college_id 
   AND s.college_dept_id = cd.college_dept_id 
   AND sy.college_dept_id = cd.college_dept_id
   AND ps.syllabus_id = sy.syllabus_id  
   AND sr.syllabus_id = sy.syllabus_id 
   AND sr.student_id = s.student_id
   AND sr.student_id=s.student_id 
   GROUP BY s.student_id 
   ORDER BY c.college_name
           ,sr.semester 
           LIMIT 5;
           
SELECT s.student_id 
  AS roll_number  
    ,s.student_name 
  AS student_name 
    ,s.dob  
    ,c.college_name 
  AS college_name  
    ,d.department_name
    ,sr.semester  
    ,sr.grade  
    ,sr.credits  
   FROM university u  
       ,college c  
       ,department d   
       ,designation de  
       ,college_department cd
       ,employee e   
       ,student s  
       ,syllabus sy  
       ,professor_syllabus ps  
       ,semester_result sr 
  WHERE c.university_code = u.university_code
   AND u.university_code = d.university_code 
   AND cd.college_id = c.college_id  
   AND cd.university_dept_code = d.department_code 
   AND e.college_id = c.college_id
   AND e.college_dept_id = cd.college_dept_id  
   AND e.designation_id = de.designation_id  
   AND s.college_id = c.college_id 
   AND s.college_dept_id = cd.college_dept_id 
   AND sy.college_dept_id = cd.college_dept_id
   AND ps.syllabus_id = sy.syllabus_id  
   AND sr.syllabus_id = sy.syllabus_id 
   AND sr.student_id = s.student_id
   AND sr.student_id=s.student_id 
   GROUP BY s.student_id 
   ORDER BY c.college_name
           ,sr.semester 
           LIMIT 5 OFFSET 10;
           
           
SELECT s.student_id 
  AS roll_number  
    ,s.student_name 
  AS student_name 
    ,s.dob  
    ,c.college_name 
  AS college_name  
    ,d.department_name
    ,sr.semester  
    ,sr.grade  
    ,sr.credits  
   FROM university u  
       ,college c  
       ,department d   
       ,designation de  
       ,college_department cd
       ,employee e   
       ,student s  
       ,syllabus sy  
       ,professor_syllabus ps  
       ,semester_result sr 
  WHERE c.university_code = u.university_code
   AND u.university_code = d.university_code 
   AND cd.college_id = c.college_id  
   AND cd.university_dept_code = d.department_code 
   AND e.college_id = c.college_id
   AND e.college_dept_id = cd.college_dept_id  
   AND e.designation_id = de.designation_id  
   AND s.college_id = c.college_id 
   AND s.college_dept_id = cd.college_dept_id 
   AND sy.college_dept_id = cd.college_dept_id
   AND ps.syllabus_id = sy.syllabus_id  
   AND sr.syllabus_id = sy.syllabus_id 
   AND sr.student_id = s.student_id
   AND sr.student_id=s.student_id 
   GROUP BY s.student_id 
   ORDER BY c.college_name
           ,sr.semester 
           LIMIT 5 OFFSET 15;
           