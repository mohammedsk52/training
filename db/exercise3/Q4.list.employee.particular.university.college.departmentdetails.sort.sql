SELECT employee.employee_name 
    AS employee_name
      ,department.department_name
      ,designation.name 
    AS designation_name
      ,college.college_name 
    AS College_Name
      ,college.city
      ,college.state 
      FROM university university
          ,college college  
          ,department department  
          ,designation designation  
          ,college_department college_dept  
          ,employee employee 
      WHERE college.university_code = university.university_code  
        AND university.university_code = department.university_code 
        AND college_dept.college_id = college.college_id  
        AND college_dept.university_dept_code = department.department_code
        AND employee.college_id = college.college_id  
        AND employee.college_dept_id = college_dept.college_dept_id  
        AND employee.designation_id = designation.designation_id  
        AND university.university_code = 1 
        ORDER BY designation.position;