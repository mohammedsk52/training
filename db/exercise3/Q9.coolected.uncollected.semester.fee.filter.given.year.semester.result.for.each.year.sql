SELECT s.student_id AS roll_number  
      ,s.student_name AS student_name  
      ,s.gender  
      ,s.dob  
      ,s.address 
      ,d.department_name  
      ,c.college_name AS college_name
      ,u.university_name  
      ,sem_fee.amount  
      ,sem_fee.paid_year  
      ,sem_fee.paid_status 
        FROM university u  
            ,college c  
            ,department d   
            ,college_department cd
            ,student s  
            ,syllabus sy  
            ,semester_fee sem_fee 
        WHERE c.university_code = u.university_code  
          AND u.university_code = d.university_code  
          AND cd.college_id = c.college_id 
          AND cd.university_dept_code = d.department_code 
          AND s.college_id = c.college_id 
          AND s.college_dept_id = cd.college_dept_id  
          AND sem_fee.student_id = s.student_id 
          AND sem_fee.college_dept_id = cd.college_dept_id 
          AND paid_year =2020  
          GROUP BY s.student_id 
          ORDER BY roll_number ;



SELECT u.university_name  
      ,s.student_id AS roll_number  
      ,s.student_name AS student_name  
      ,s.gender  
      ,s.dob  
      ,s.address  
      ,c.college_name AS college_name  
      ,d.department_name
      ,sem_fee.amount  
      ,sem_fee.paid_year  
      ,sem_fee.paid_status 
        FROM university u  
            ,college c  
            ,department d   
            ,college_department cd  
            ,student s
            ,syllabus sy  
            ,semester_fee sem_fee 
        WHERE c.university_code = u.university_code  
          AND u.university_code = d.university_code  
          AND cd.college_id = c.college_id 
          AND cd.university_dept_code = d.department_code 
          AND s.college_id = c.college_id 
          AND s.college_dept_id = cd.college_dept_id 
          AND sy.college_dept_id = cd.college_dept_id 
          AND sem_fee.student_id = s.student_id
          AND sem_fee.college_dept_id = cd.college_dept_id 
          AND u.university_code=1 
          AND paid_year = 2020 
          GROUP BY s.student_id 
          ORDER BY roll_number ; 
