/*
-- Query: select college.college_code,college.college_name,(select distinct university_name from university u where u.university_code=college.university_code limit 1), 
college.city,college.state,college.year_opened,
(select distinct department_name from department department where department.university_code=college.university_code limit 1),
(select distinct name from designation where name='HOD') from college college,university u where 
college.university_code=u.university_code
LIMIT 0, 1000

-- Date: 2020-08-07 22:23
*/
INSERT INTO `` (`college_code`,`college_name`,`(select distinct university_name from university u where u.university_code=college.university_code limit 1)`,`city`,`state`,`year_opened`,`(select distinct department_name from department department where department.university_code=college.university_code limit 1)`,`(select distinct name from designation where name='HOD')`) VALUES ('10','kpr institute','anna university','coimbatore','tamil nadu',2010,'cse','hod');
INSERT INTO `` (`college_code`,`college_name`,`(select distinct university_name from university u where u.university_code=college.university_code limit 1)`,`city`,`state`,`year_opened`,`(select distinct department_name from department department where department.university_code=college.university_code limit 1)`,`(select distinct name from designation where name='HOD')`) VALUES ('20','CIT','anna university','coimbatore','tamil nadu',1973,'cse','hod');
INSERT INTO `` (`college_code`,`college_name`,`(select distinct university_name from university u where u.university_code=college.university_code limit 1)`,`city`,`state`,`year_opened`,`(select distinct department_name from department department where department.university_code=college.university_code limit 1)`,`(select distinct name from designation where name='HOD')`) VALUES ('30','Sakthi institute','anna university','salem','tamil nadu',1989,'cse','hod');
INSERT INTO `` (`college_code`,`college_name`,`(select distinct university_name from university u where u.university_code=college.university_code limit 1)`,`city`,`state`,`year_opened`,`(select distinct department_name from department department where department.university_code=college.university_code limit 1)`,`(select distinct name from designation where name='HOD')`) VALUES ('40','Hindusthan institute','cept','chennai','tamil nadu',1988,'cse','hod');
INSERT INTO `` (`college_code`,`college_name`,`(select distinct university_name from university u where u.university_code=college.university_code limit 1)`,`city`,`state`,`year_opened`,`(select distinct department_name from department department where department.university_code=college.university_code limit 1)`,`(select distinct name from designation where name='HOD')`) VALUES ('50','PSR institute','cept','chennai','tamil nadu',1992,'cse','hod');
