SELECT student.student_id 
  AS roll_number
    ,student.student_name 
  AS student_name  
    ,student.gender  
    ,student.dob 
    ,student.email
    ,student.phone 
    ,student.address
    ,college.college_name
    ,department.department_name
    ,student.accademic_year  
   FROM student student
      ,college college
      ,university university 
      ,department department 
      ,college_department college_dept 
   WHERE student.accademic_year = '2021' 
     AND university.university_code = '1' 
     AND college.city ='coimbatore'
     AND college.university_code = u.university_code  
     AND university.university_code = d.university_code 
     AND college_dept.college_id = c.college_id  
     AND college_dept.university_dept_code = department.department_code
     AND student.college_id = college.college_id
     AND student.college_dept_id = college_dept.college_dept_id ;