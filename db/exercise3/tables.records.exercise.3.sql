use exercise3;
insert into university 
 values(1,'anna university');
insert into university
 values(2,'cept university');
 select*from university;
insert into department 
 values (11,'cse',1);
insert into department 
 values (12,'ece',1);
insert into department 
 values (21,'cse',2);
insert into department 
 values (13,'mech',1);
insert into department 
 values (24,'eee',2);
insert into department 
 values (14,'eee',1);
insert into department 
 values (22,'ece',2); 
insert into department 
 values (23,'mech',2); 
 select*from department;
insert into college
 values(1,10,'kpr institute',1,'coimbatore','tamil nadu',(2010));
insert into college
 values(2,20,'CIT',1,'coimbatore','tamil nadu',(1973));
insert into college
 values(3,30,'Sakthi institute',1,'salem','tamil nadu',(1989)); 
insert into college
 values(4,40,'Hindusthan institute',2,'chennai','tamil nadu',(1988)); 
insert into college
 values(5,50,'PSR institute',2,'chennai','tamil nadu',(1992)); 
 SELECT*FROM college;
insert into designation
 values(1,'hod',1);
insert into designation
 values(2,'professor',1);
insert into designation
 values(3,'assistant professor',1);
insert into designation
 values(4,'office staff',1);
insert into designation
 values(5,'hod',2);
insert into designation
 values(6,'professor',2);
insert into designation
 values(7,'assistant professor',2);
insert into designation
 values(8,'office staff',2);
insert into designation
 values(9,'hod',3);
insert into designation
 values(10,'professor',3);
insert into designation
 values(11,'assistant professor',3);
insert into designation
 values(12,'office staff',3);
insert into designation
 values(13,'hod',4);
insert into designation
 values(14,'professor',4);
insert into designation
 values(15,'assistant professor',4);
insert into designation
 values(16,'office staff',4); 
insert into designation
 values(17,'hod',5);
insert into designation
 values(18,'professor',5);
insert into designation
 values(19,'assistant professor',5);
insert into designation
 values(20,'office staff',5);
 select*from designation;
insert into college_department 
 values (1,11,1);
insert into college_department 
 values (2,12,1);
insert into college_department 
 values (3,13,1);
insert into college_department 
 values (4,14,1);
insert into college_department 
 values (5,11,2);
insert into college_department 
 values (6,12,2);
insert into college_department 
 values (7,13,2);
insert into college_department 
 values (8,14,2);
insert into college_department 
 values (9,11,3);
insert into college_department 
 values (10,12,3);
insert into college_department 
 values (11,13,3);
insert into college_department 
 values (12,14,3);
insert into college_department 
 values (13,21,4);
insert into college_department 
 values (14,22,4);
insert into college_department 
 values (15,23,4);
insert into college_department 
 values (16,24,4); 
insert into college_department 
 values (17,21,5);
insert into college_department 
 values (18,22,5);
insert into college_department 
 values (19,23,5);
insert into college_department 
 values (20,24,5);
 select*from college_department;
 insert into employee
 values(1,'taha',str_to_date('04-05-1967' , '%d-%m-%Y'),'taha53@gmail.com',9384784053,1,1,1);
insert into employee
 values(2,'mayur',str_to_date('14-02-1970' , '%d-%m-%Y'),'mayur22@gmail.com',80004084858,1,2,2);
insert into employee
 values(3,'praveen',str_to_date('26-09-1975' , '%d-%m-%Y'),'praveen@gmail.com',8846205670,1,3,3);
insert into employee
 values(4,'sabri',str_to_date('03-03-1969' , '%d-%m-%Y'),'sabri678@gmail.com',6543108712,1,4,4); 
insert into employee
 values(5,'mervin',str_to_date('25-11-1977' , '%d-%m-%Y'),'mervincool@gmail.com',9645315679,2,5,9);
insert into employee
 values(6,'sridhar',str_to_date('01-06-1979' , '%d-%m-%Y'),'sridhar1790@gmail.com',9345102657,2,6,10);
insert into employee
 values(7,'mohsin',str_to_date('16-07-1970' , '%d-%m-%Y'),'mohsin56@gmail.com',6350037891,2,7,11);
insert into employee
 values(8,'nithi',str_to_date('02-06-1980' , '%d-%m-%Y'),'nithi@gmail.com',9023466870,2,8,12);
insert into employee
 values(9,'mukesh',str_to_date('09-08-1990' , '%d-%m-%Y'),'mukesh06@gmail.com',9631482590,3,9,13);
insert into employee
 values(10,'kavipriya',str_to_date('24-12-1970' , '%d-%m-%Y'),'kavi240@gmail.com',9886443987,3,10,14);
 insert into employee
 values(11,'murtaza',str_to_date('04-05-1979' , '%d-%m-%Y'),'murtaza@gmail.com',8811756704,3,11,15);
insert into employee
 values(12,'pradeep',str_to_date('02-07-1978' , '%d-%m-%Y'),'pradeep@gmail.com',9823467970,3,12,16);
insert into employee
 values(13,'priya',str_to_date('25-03-1976' , '%d-%m-%Y'),'priya@gmail.com',9842121590,4,13,5);
insert into employee
 values(14,'prasath',str_to_date('16-06-1978' , '%d-%m-%Y'),'prasath567@gmail.com',9142713495,4,14,6);
insert into employee
 values(15,'sejal',str_to_date('02-06-1975' , '%d-%m-%Y'),'sejal789@gmail.com',9723574629,4,15,7);
insert into employee
 values(16,'rakshith',str_to_date('30-03-1970' , '%d-%m-%Y'),'rakhshi67@gmail.com',9641239879,4,16,8);
insert into employee
 values(17,'karthik',str_to_date('25-01-1976' , '%d-%m-%Y'),'karthik@gmail.com',974128690,5,17,17);
insert into employee
 values(18,'nisha',str_to_date('13-12-1973' , '%d-%m-%Y'),'nisha9087@gmail.com',9214200379,5,18,18);
insert into employee
 values(19,'kanishka',str_to_date('03-07-1974' , '%d-%m-%Y'),'kanishka@gmail.com',9775436965,5,19,19);
insert into employee
 values(20,'rushang',str_to_date('31-01-1972' , '%d-%m-%Y'),'rushang786@gmail.com',9641003977,5,20,20);
select*from employee;