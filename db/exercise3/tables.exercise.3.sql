use exercise3;
create table university(university_code char(25) not null primary key,university_name varchar(25));
select*from univeristy;
create table department(deptartment_code char(25) not null primary key,department_name varchar(25),university_code char(25),foreign key(university_code) references university(university_code));
select*from department;
create table college(college_id int not null primary key,college_code char(25),college_name varchar(25),university_code char(25),city varchar(50),state varchar(50),year_opened year,foreign key(university_code) references university(university_code));
select*from college;
create table designation(designation_id int not null primary key,name varchar(25),position varchar(2));
select*from designation;
create table college_department(college_dept_id int not null primary key,university_dept_code char(25),college_id int,foreign key(college_id) references college(college_id),foreign key(university_dept_code) references department(department_code));
select*from college_department;
create table employee(employee_id int not null primary key,employee_name varchar(25),dob date,email varchar(25),phone bigint,college_id int,college_dept_id int,designation_id int,foreign key(college_id) references college(college_id),foreign key(college_dept_id) references college_department(college_dept_id),foreign key(designation_id) references designation(designation_id));
select*from employee;
create table syllabus(syllabus_id int not null primary key,college_dept_id int,syllabus_code char(4),syllabus_name varchar(100),foreign key(college_dept_id) references college_department(college_dept_id));
select*from syllabus;
create table professor_syllabus(emp_id int not null primary key,syllabus_id int,semester tinyint,foreign key(syllabus_id) references syllabus(syllabus_id));
select*from professor_syllabus;
create table student(student_id int not null primary key,student_name varchar(25),dob date,gender char(6),email varchar(25),phone bigint,address varchar(200),accademic_year year(4),college_dept_id int,college_id int,foreign key(college_id) references college(college_id),foreign key(college_dept_id) references college_department(college_dept_id));
select*from student;
create table semester_fee(college_dept_id int,student_id int,semester tinyint,amount double(18,2) default 30000,paid_year year(4) default Null,paid_status varchar(10) default 'unpaid',foreign key(student_id) references student(student_id),foreign key(college_dept_id) references college_department(college_dept_id));
select*from semester_fee;
create table semester_result(student_id int,syllabus_id int,semester tinyint,grade varchar(2),credits float,result_date date,foreign key(syllabus_id) references syllabus(syllabus_id),foreign key(student_id) references student(student_id));
select*from semester_result;
