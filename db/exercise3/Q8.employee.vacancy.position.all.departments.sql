SELECT de.name AS designation_name   
      ,position  
      ,d.department_name  
      ,c.college_name AS college_name    
      ,u.university_name  
      ,c.city
      ,c.state
        FROM university u  
            ,college c  
            ,department d   
            ,designation de  
            ,college_department cd  
            ,employee e  
        WHERE c.university_code = u.university_code 
          AND u.university_code = d.university_code  
          AND cd.college_id = c.college_id  
          AND cd.university_dept_code = d.department_code 
          AND e.college_id = c.college_id 
          AND e.college_dept_id = cd.college_dept_id  
          AND e.designation_id = de.designation_id  
          GROUP BY de.name 
          ORDER BY position;