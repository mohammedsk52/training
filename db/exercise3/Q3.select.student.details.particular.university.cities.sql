SELECT student.student_id 
  AS roll_number 
    ,student.student_name 
  AS student_name 
    ,student.gender 
    ,student.dob 
    ,student.email
    ,student.phone
    ,student.address 
    ,college.college_name 
    ,department.department_name
    ,employee.employee_name 
  AS employee_name 
   FROM university university
       ,college college 
	   ,department department  
	   ,college_department college_dept 
	   ,student student
	   ,employee employee
	   ,designation designation 
   WHERE college.university_code = university.university_code  
     AND university.university_code = department.university_code 
     AND college_dept.college_id = college.college_id 
     AND college_dept.university_dept_code = department.department_code 
     AND employee.college_id = college.college_id  
     AND employee.college_dept_id = college_dept.college_dept_id 
     AND student.college_dept_id = college_dept.college_dept_id 
     AND employee.employee_id = designation.designation_id  
     AND designation.name = 'hod' 
     AND university.university_code = '1'  
     AND college.city ='coimbatore';