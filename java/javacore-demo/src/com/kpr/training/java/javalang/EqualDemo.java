/*
Requirement: To demonstrate object equality using Object.equals() vs ==, using String objects
Entity: EqualDemo
Function Declaration: public static void main(String[] args)
Jobs To Be Done:-
                 1) An object has to be created for two string string1 and string
                 2) Then the object is compared using '==' operator if it is true if block gets executed
                    otherwiswe else block gets executed
                 3) Then the object is compared using equals if it is true if block gets executed
                    otherwiswe else block gets executed

*/
package com.kpr.training.java.javalang;

public class EqualDemo { 

    public static void main(String[] args) 
    { 
        String string = new String("wild"); 
        String string1 = new String("wild"); 
        if(string == string1) {
            
            System.out.println("True");
        
        } else {
        
           System.out.println("False");
        
        }
        if(string.equals(string1)) {
            
            System.out.println("True");
        
        } else {
            
            System.out.println("False");
        
        }
    } 
}