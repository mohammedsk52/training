/*
Requirement: To create a program that is similar to the previous one but instead of reading integer 
             arguments,it reads floating-point arguments.It displays the sum of the arguments, using 
             exactly two digits to the right of the decimal point.
Entity: FloatingPointAdder
Function declaration: public static void main(String[] args).
Jobs to be done:
               1) Import the package java.text.DecimalFormat.
               2) Create a class FloatingPointAdder
               3) Declare float variable a,b and c to store the values
               4) use if condition to check the length of the arguments 
               5) if length is less than 2 it will print as error
                  otherwise else block will be executed
               6) Print the output in the correct format. 
*/
package com.kpr.training.java.datatype;

import java.text.DecimalFormat;

public class FloatingPointAdder {
    public static void main(String[] args) {
        
        float a = Float.parseFloat(args[0]);
        float b = Float.parseFloat(args[1]);
        float c = Float.parseFloat(args[2]);
        if (args.length < 2) {
            
            System.out.println("Error");
        
        } else {
        
        double sum = 0.0;
        for (int i = 0; i < args.length; i++) {
            
            sum += Double.valueOf(args[i]).doubleValue();
        
        }
        DecimalFormat correctFormat = new DecimalFormat("###,###.##");
        String output = correctFormat.format(sum);
            System.out.println(output);
        }
    }
}