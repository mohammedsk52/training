/*
Requirement:
    To demonstrate overloading with Wrapper types

Entity:
    WrapperOverLoading 

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)An object is created for the class WrapperOverLoading.
    2)Invoke the method overLoad from the class WrapperOverLoading and pass a parameter of  wrapper
     type  Integer.
      2.1)The Integer value is printed.
    3)Invoke the method overLoad from the class WrapperOverLoading and pass a parameter of wrapper
      type Float.
      3.1)Print the Float value
*/

package com.kpr.training.java.datatype;

public class WrapperOverLoading {

    public void overLoad(Integer value) {    
        System.out.println("Integer : " + value);
    }
    
    public void overLoad(Float value) {    
        System.out.println("Float : " + value);
    }
    
    public static void main(String[] args) {
        
        WrapperOverLoading wrapperOverLoading = new WrapperOverLoading();
        wrapperOverLoading.overLoad(new Integer(2));
        wrapperOverLoading.overLoad(new Float(2.6f));   
    }
}