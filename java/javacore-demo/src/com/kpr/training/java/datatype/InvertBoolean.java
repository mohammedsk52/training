/*
Requirement: To invert the value of a boolean,using an operator.
Entity: InvertBoolean
Function declaration: public static void main(String[] args)
Jobs to be done:
    1)word is declared as boolean .
    2)Two string are checked whether they are equal and the result is inverted and saved
      as boolean in word
    3)The value in word is printed
*/

//Program:
package com.kpr.training.java.datatype;
public class InvertBoolean {
    public static void main(String[] args) {
        boolean word = !("hello" == "hello"); 
        System.out.println("Inverted boolean value:" + word);
    }
}