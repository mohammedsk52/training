/*
Requirement: print the type of the result value of following expressions
            - 100 / 24
            - 100.10 / 10
            - 'Z' / 2
            - 10.5 / 0.5
            - 12.4 % 5.5
            - 100 % 56
Entity:
    DataType
    
Function Declaration:
    public static void main(String[] args)
    
Jobs To Be Done:
    1)Create a object o and store the result of 100 / 24
    2)Type of o is printed .
    3)Create a object a and store the result of 100.10 / 10
    4)Type of a is printed .
    5)Create a object b and store the result of 'Z' / 2
    6)Type of b is printed .
    7)Create a object c and store the result of 10.5 / 0.5
    8)Type of c is printed .
    9)Create a object d and store the result of 12.4 % 5.5
    10)Type of d is printed .
    11)Create a object e and store the result of 100 % 56
    12)Type of e is printed .
*/
package com.kpr.training.java.datatype;
public class DataType
{
	public static void main(String[] args) {
		
        Object o = 100 / 24;
	    System.out.println(o.getClass().getName());
	    Object a = 100.10 / 10;
	    System.out.println(a.getClass().getName());
	    Object b = 'Z' /2;
	    System.out.println(b.getClass().getName());
	    Object c =10.5 /0.5 ;
	    System.out.println(c.getClass().getName());
	    Object d = 12.4 % 5.5;
	    System.out.println(d.getClass().getName());
	    Object e = 100/56;
	    System.out.println(e.getClass().getName());
	}
}