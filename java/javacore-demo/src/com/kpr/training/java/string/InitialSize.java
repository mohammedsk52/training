/*
Requirement:
    What is the initial capacity of the following string builder?
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");

Entity:
    InitialSize

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1)StringBuider is created named sb and stores "Able was I ere I saw Elba."
    2)Length of the sb is printed.
*/
/*
Intial size:16
stringbuilder lenght:26
total size:16+26=42
*/

package com.kpr.training.java.string;
 
import java.util.*;

public class InitialSize {
    
    public static void main(String[] args) {
        
        StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");
        System.out.println(sb.length());//OUTPUT : 26//
    }
}