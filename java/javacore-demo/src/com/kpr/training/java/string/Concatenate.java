/*
Requirement:
    Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
       String hi = "Hi, ";
       String mom = "mom.";
Entites:
    Concatenate
Function Declaration: 
    public static void main(String[] args)

Jobs To Be Done:
    1) create the concatenate class
    2) Declare and initialise the string hi and mom as "HI" and "mom."
    3) Declare string s and concatenate string hi and mom using + operator.
    4) Print the string s.
    5) Declare the string h and concatenate hi and mom using concat.
    6) Print the string h.
*/     
package com.kpr.training.java.string;     
import java.util.*;
public class Concatenate{
    public static void main(String[] args){
        String hi = "Hi, ";
        String mom = "mom.";
        String  s = hi + mom ;
        System.out.println("concatenated string:"+s);
        String h= hi.concat(mom);
        System.out.println("concatenated string:"+h);
    }
}

    