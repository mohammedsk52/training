/*
Requirement: How long is the string returned by the following expression? What is the string?
            "Was it a car or a cat I saw?".substring(9, 12)
Entity: SubStringDemo
Function Declaration: public static void main(String[] args)
Jobs To Be Done:
               1) create the SubStringDemo
               2) string of type string has been declared
               3) string is intialized
               4) substring function is used to print the substring value
*/
/*
   solution:cat
*/
package com.kpr.training.java.string;
import java.lang.String;
public class SubStringDemo {
    public static void main(String[] args) {
        String string;
        string = "Was it a car or a cat I saw?" ;
        String s = string.substring(9,12) ;
        System.out.println("substring:" + s );//ouput:car
    }
}