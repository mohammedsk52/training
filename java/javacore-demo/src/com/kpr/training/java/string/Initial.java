/*
Requirement: To print the initial from the full name
Entities: Initial
Function Declaration: NO fuction declaration
Jobs To Be Done: 
                1) create the intial class
                2) Get the name as input from the user
                3) The lenght of the name is found
                4) The string is iterated
                5) For every iteration each letter comes into the loop
                6) If the letter is in upper case it is printed
*/               
package com.kpr.training.java.string;
import java.util.*;
public class Initial{
    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter the name:");
        String name = sc.nextLine();
        int length = name.length();
        System.out.print("Initial:");
        for (int i = 0; i < length; i++) {
            
            if (Character.isUpperCase(name.charAt(i))) {
             
                System.out.print(name.charAt(i)+" ");
            
            }
        }
       
    }
}