/*
Requiremnt: To sort string[] alphabetically ignoring case and to print even index indexed into uppercse
Entities: SortString
Function Declaration: public static void main(String[] args)
Jobs To Be Done:
                1) create the SortString class
                2) Array containing string is created
                3) Array is sorted using sort()
                4) The array is iterated 
                5) If the index of the array is divisible by two the particular string in that index is
                   converted to uppercase and then updated in the array
                6) Print the output with the updated array
 */
package com.kpr.training.java.string;
import java.util.Arrays;
class SortString{
    
    public static void main(String[] args) {
      
        String[] array={ "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(array,String.CASE_INSENSITIVE_ORDER);
        System.out.print("sorted list:");
        System.out.println(Arrays.asList(array));
        for(int i = 0; i < array.length; i++) {
           
            if(i%2 == 0){
                
               array[i] = array[i].toUpperCase();
            }
           
        } 
        System.out.println("even index in uppercase:");
        
        System.out.println(Arrays.asList(array));
       
    }
}