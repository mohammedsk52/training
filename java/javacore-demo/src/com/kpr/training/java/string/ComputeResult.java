/*
Requirement:To find the output of the following program after each numbered line.
Entity:ComputeResult
Function declaration:public static void main(String[] args)
Jobs to be done:
     1) Create the class ComputeResult
     2) Declaring the string variable original as software.
     3) Create the object as result assigning hi  to it.
     4) Declaring the variable index of type int and assiging the index of 'a' in the string original
        charat(0)  sets the first character of the string original 'h' to 's'.
     5) Then i is removed and e is set as 'i' is the character in the final result value.
     6) w is inserted at first index using insert
     7) result is appened by the substring from the original
     8) ar is added in second index and a space is added
     9) print the resut
*/
/*
Output:
1) si
2) se
3) swe 
4) sweoft
5) swear oft
*/
package com.kpr.training.java.string;

public class ComputeResult {
        public static void main(String[] args) {
            
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');
            result.setCharAt(0, original.charAt(0));//result:si
            result.setCharAt(1, original.charAt(original.length()-1));//result:se
            result.insert(1, original.charAt(4));//result:swe
            result.append(original.substring(1,4));//result:sweoft
            result.insert(3, (original.substring(index, index+2) + " "));//result:swear oft
            System.out.println(result);
        
        }
    }

