/*
Requirement: To demonstrate overloading with varArgs
Entity: VarargDemo ,VariableArgument
Function declaration: public static void main(String[] args)
                      public void printNumbers(int... number)
Jobs to be done:-
                1) In the main class an object is cretaed for varaiableargument class
                2) Then the method printNumbers  is called in the main method with three parameter
                3) Again the method printNumbers  is called in the main method with ten parameter where
                   overloading is acheived.
                4) In the method print number the numbers are printed
*/
package com.kpr.training.java.vararg;
class VariableArgument{
    public void printNumbers(int... number) {
        System.out.println("The numbers are:");
        for(int i : number) {
        System.out.println(i);
        }
    }
   
}
public class VarargDemo{
    public static void main(String[] args) {
        
        VariableArgument variableArgument = new VariableArgument();
        variableArgument.printNumbers(1,2,3);
        variableArgument.printNumbers(1,2,3,4,5,6,7,8,9,10);
        
    }
}