/*
Requirement:
    To identify the output of the following program
    IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
Entities:
    IdentifyMyParts
Function Declaration:
    There is no function declaration
Jobs to be done:
    1) check the values for x and y for the object a and b.
    2) The result will be printed according to it.
*/
OUTPUT:
a.y = 5
b.y = 6
a.x = 1
b.x = 2
IdentifyMyParts.x = 2 