/*
Requirement:
     Change the following program to use compound assignments:
       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }
Entities:
     ArithmeticDemo
Function Declaration:
     public static void main (String[] args)
Jobs to be done:
    1)Declare a variable result of type int and store 3 in it.
    2)Print the result.
    3)Then subtract 1 from the result .
    4)Print the result.
    5)Multiply 2 to the result .
    6)Print the result .
    7)Divide the result by 2.
    8)Print the result.
    9)Add 8 to the result .
    10)Print the result .
    11)Apply modulo by 7 to the result.
    12)Print the result.
*/
package com.kpr.training.java.operator;
class ArithmeticDemo {

    public static void main (String[] args){
        int result = 3; // result is now 3
        System.out.println(result);

        result -= 1; // result is now 2
        System.out.println(result);

        result *= 2; // result is now 4
        System.out.println(result);

        result /= 2; // result is now 2
        System.out.println(result);

        result += 8; // result is now 10
        System.out.println(result);
        result %= 7; // result is now 3
        System.out.println(result);

    }
}
