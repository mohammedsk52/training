/*
Requirement: To print the fibonacci series using for loop
Entities: FibonacciFor
Function Declaration: public static void main(String[]args)
Jobs To Be Done:
    1)Declare the limit as int and assign 10 to it .
    2)Declare the result as int and assign 0 to it .
    3)Declare the temporaryValue as int and assign 0 to it .
    4)Declare sum as int .
    5)DEcalre number as int .
    6)For each number till the limit 
      6.1)print result
      6.2)Add result and temporaryValue and store it in sum.
      6.3)Store the temporaryValue in result .
      6.4)Store the sum in temporaryValue
*/  
package com.kpr.training.java.controlflow;

public class FibonacciFor {
    public static void main(String[]args) {
       
       int limit = 10 ;
       int a = 0 ;
       int b = 1 ;
       int sum ;
       int i;
       System.out.println("Fibonacci series:");
       for(i = 1; i < limit; ++i) {
            System.out.print(a+" , ");
            sum = a + b;
            a = b;
            b = sum ;
       }
    }
}