/*Requirement:
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities: Test

Function Declaration: public static void main (String args[])

Jobs To Be Done:
                1) consider the value of aNumber as 3
                2) It checks for the condition aNumber is greater than or equal to zero                 
                3) The condition is satisfied so that it checks for next statement 
                4) It checks for the condition aNumber is equal to zero it is not satisfied 
                5) so that it comes out of if and then print statement is executed.
                6) The program is written with correct format.
*/
/*OUTPUT:
third string
*/
package com.kpr.training.java.controlflow;

class Test {
    
    public static void main (String args[]) {
        
        int aNumber = 3;
        if (aNumber >= 0) {
            
            if (aNumber == 0) {
                
                System.out.println("first string");
            }
        } else {
            
                System.out.println("second string");
        }
    System.out.println("third string");
    }
}
