/*
Requiremnet: To print the fibonacci series using while loop
Entities: FibonacciWhile
Function Declaration: public static void main(String[] args
Jobs To Be Done:
               1) create the FibbonaciWhile class
               2) Get the limit from the user.
               3) Initialize a,b and i as 0,1and 0
               4) check for the condition i is less than the limit.
               5) sum is assigned as i if i is less than or equal to one
                  Otherwise a and b are summed up and updated in sum
               6) b is assigned to a and sum is assigned to b.
               7) print the value of the sum
               8) i is incremented by one.
               9) The loop contimues till the condition i.e i less than the limit is true.
                
*/
package com.kpr.training.java.controlflow;
class FibonacciWhile {
	
    public static void main(String[] args){
        
        int a = 0 ;
        int b = 1 ; 
        int i = 0 ;
        int sum ;
        int limit = 10;
        System.out.print("Fibonacci series:");
        while(i<limit){
            
            if(i<=1){
             
             sum=i;
            
            }else{
             
             sum=a+b;
              a=b;
              b=sum;
            
            }
         System.out.print(sum+" ,");
         i++;
        }
    }
}