/*
Requirement: To print fibonacci series using recursion
Entities: FibonacciRecursion
Function Declaration: public void fibonacci
                                 public static void main(String[] args)
Jobs To Be Done:
    1)Declare the variable number as int and assign 0 to it.
    2)Declare the variable value1 as int and assign 0 to it.
    3)Declare the variable value2 as int and assign 1 to it.
    4)Declare the variable sum a int.
    5)1)Declare the variable limit as int and assign 10 to it.
    6)Invoke the method fibonacci in the class FibonacciRecursion and pass limit as the parameter.
    7)Check the limit is greater than 0.
      7.1)Check the number is less than or equal to 1.
          7.1.1)Assign number to sum
      7.2)If the condition 7.1 is not satisfied
          7.2.1)Add value1 and value2 and store it in sum.
          7.2.2)Assign the value2 to value1
          7.2.3)Assign the sum to value2.
    
      7.3)Print the sum .
      7.4)Increment the number by 1.
      7.5)Invoke the fibonacci method from FibonacciRecursion and reduce the limit by one and pass 
          as parameter.
*/
package com.kpr.training.java.controlflow;
public class FibonacciRecursion {
	int i = 0;
    int a = 0;
    int b = 1;
    int sum;
	public void fibonacci(int n) {
	    if(n > 0) {
	        if(i <= 1) {
	       
              sum = i;
            
            } else {
	    	
              sum = a + b;
	    	  a = b;
	    	  b = sum;
	        }
	        System.out.print(sum + " ,");
	        i++;
	        fibonacci(--n);
	    }
    }
	public static void main(String[] args) {
	   
	    int limit = 10;
	    FibonacciRecursion fibonacciRecursion=new FibonacciRecursion();
	    System.out.print("Fibonacci series:");
	    fibonacciRecursion.fibonacci(limit);
	
    }
}