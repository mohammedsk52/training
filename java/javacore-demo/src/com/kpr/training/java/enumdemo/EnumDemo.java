/*
Requirement:
    compare the enum values using equal method and == operator

Entity:
   EnumDemo
   Months

Function Declaration:
   public static void main(String[] args)

Jobs To Be Done:
    1)In the enum Months store months
    2)Create a variable month of type Months and store the value May
    3)Compare the month with the january in the enum Months
      3.1)If the condition is satisfied print as january has 31 days. 
    4)Compare the month with the february in the enum Months
      4.1)If the condition is satisfied print as january has 29 days
    5)Compare the month with the march in the enum Months
      5.1)If the condition is satisfied print as march has 31 days
    6)Compare the month with the april in the enum Months
      6.1)If the condition is satisfied print as april has 30 days
    7)Compare the month with the may in the enum Months
      7.1)If the condition is satisfied print as may has 29 days
      
*/

package com.kpr.training.java.enumdemo;

public class EnumDemo {
    
    public enum Months {JANUARY
                       ,FEBRUARY
                       ,MARCH
                       ,APRIL
                       ,MAY
                       }
   
    public static void main(String[] args) {
       
          Months month = Months.valueOf("MAY");
      
        if(month.equals (Months.JANUARY)) {
            
            System.out.println("JANUARY has 31 days");
        
        }else if(Months.FEBRUARY == month) {
           
           System.out.println("FEBRUARY has 29 days");
        
        } else if(Months.MARCH == month) {
           
           System.out.println("MARCH has 31 days");
        
        }else if(month.equals(Months.APRIL)) {
            
            System.out.println("APRIL has 30 days");
       
       }else if(Months.MAY == month) {
        
        System.out.println("MAY has 31 days");
        
        }       
    }
}