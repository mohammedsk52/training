/*Requirements:-
 *   To write a program to print 1-7 number using While loop
 *syntax:-
 *    while(condition){  
          
    } 
  
 * Jobs to be done:-
 * 1) create a whileLoop class
 * 2) initialize the i value to 1
 * 3) using while loop write the test condition as i greater than equal to 7
 * 4) print the values of i till the condition is true
 * 5) increment the value of i by 1  
 */
package com.training.exercise1.loops;
public class WhileLoop {  
public static void main(String[] args) {  
    int i=1;  
	//while condition
    while(i<=7){  
        System.out.println(i);  
    i++;  
    }  
}  
}  