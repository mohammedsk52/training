/*Requirements:-
 * To write a program to print 1-5 number using Do-while loop
 *syntax:-
 * do{  
        }while(condition);  
 * Jobs to be done:-
 * 1) create a DoWhileLoop class
 * 2) initially set i=1
 * 3) now loop the value of i with the condition i greater than equal to 5
 * 4)print the value of i
 */
package com.training.exercise1.loops;
public class DoWhileLoop{  
public static void main(String[] args) {  
    int i=1;
     //do-while loop	
    do{  
        System.out.println(i);  
    i++;  
    }while(i<=5);  
}  
}  