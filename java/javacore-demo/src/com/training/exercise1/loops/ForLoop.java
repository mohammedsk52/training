/*Requirements:-
 *   To write a program to print 1-10 number using For loop
 *syntax:-
 *   for(initialization;test condition;increment)
  
 * Jobs to be done:-
 * 1) create a ForLoop class
 * 2) using for loop
 *   2.1) initialize the i value to 1
 *   2.2) write the test condition as i greater than equal to 10
 *   2.3) increment the value of i
 * 3) print the values till loop condition is satisfied.
 */
package com.training.exercise1.loops;
public class ForLoop {  
public static void main(String[] args) {  
    // for loop  
    for(int i=1;i<=10;i++){  
        System.out.println(i);  
    }  
}  
}  