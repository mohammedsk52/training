/* Requirements:-
 * To create a program containing inheritance,overloading,encapsulation,abstraction,constructors.
 * 
 * Jobs to be done:-
 * 1) create the class as one
 * 2) class one inheritates class two
 * 3) overload int add method
 * 4) create a class named encapsulation 
 * 5) encapsulate name and use getName to get the name 
 * 6) create a abstract class named two wheeler
 * 7) two wheeler class inherits Honda
 * 8) create the main  class
 * 9) print and display output of all the above class 
 */
package com.training.exercise1.bike;
class One {

public void display() {

System.out.println("One");

}

}

//inheritance

class Two extends One {

@Override

public void display() {             //default constructor

System.out.println("Two");

}

public int add(int x, int y) {

return x+y;

}

//Overload

public double add(double x,double y) {      //parameterized constructor

return x+y;

}

}

//encapsulation example

class EncapTest {

private String name;

public String getName() {

return name;

}

public void setName(String newName) {

name = newName;

}

}

//abstraction

abstract class TwoWheeler {

public abstract void run();

}

class Honda extends TwoWheeler{

public void run(){

System.out.println("\nbike is Running..");

}

}

class MainClass {

public static void main(String[] args) {

One a=new One();

a.display();

Two b=new Two();

b.display();

System.out.println(b.add(5,7));

System.out.println(b.add(4.,3.)); //polymorphism

EncapTest encap = new EncapTest();

encap.setName("Mohammed's");

System.out.print("Name : " + encap.getName() );

TwoWheeler test = new Honda();

test.run();

}

}