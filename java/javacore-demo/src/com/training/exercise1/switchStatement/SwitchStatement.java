/*Requirements:-
 *   To write a java program to print the given number using switch statements
 *syntax:-
     switch(condition){  
       //Case statements  
           case 1: statement1;  
              break;  
            case 2: statement2;  
              break;  
            case 3: statement3;    
              break;   
            default:statement;  
     }  
  
 * Jobs to be done:-
 * 1) create a SwitchStatement class
 * 2) define the number=20
 * 3) using switch condition
 *    3.1) write the case 1 as number is 10
 *    3.2) write the case 2 as number is 20
 *    3.3) write the case 3 as number is 30
 *    3.4) write the default as (Not in 10, 20 or 30)
 * 4) print the values if switch condition is satisfied
 * 5) break if the condition is satisfied
 */
package com.training.exercise1.switchStatement;
public class SwitchStatement {  
public static void main(String[] args) {   
    int number=20;  
    //Switch statements
    switch(number){  
    //Case statements  
    case 1: System.out.println("10");  
    break;  
    case 2: System.out.println("20");  
    break;  
    case 3: System.out.println("30");  
    break;   
    default:System.out.println("Not in 10, 20 or 30");  
    }  
}  
}  