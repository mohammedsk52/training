/*Requirements
 *  To write a program to print age is greater than 18 using if atatement
 *Syntax:-
 *  if(condition)
 *Jobs to be done:-
 * 1) create a class named if statement
 * 2) check whether age greater than 18
 * 3) print the output based on condition
 */
package com.training.exercise1.ifStatements;
public class IfStatement {  
public static void main(String[] args) {  
    
    int age=20;  
   //if statement  
    if(age>18){  
        System.out.print("Age is greater than 18");  
    }  
}  
}  