/*Requirement:-
 *  To create a interface animal class and implement in the Mammalint class
 * function declaration:-
 *  interface Animal
 * Entities:-
 *  public void eat();
 *  public void travel();
 * Jobs to be done:- 
 * 1) create a interface class named animal
 * 2) create two method named eat and travel
 * 3) Then create a mammal class that implements animal interface class
 * 4) finally print using eat and travel method
*/
package com.training.exercise1.animal;
interface Animal {
   public void eat();
   public void travel();
}