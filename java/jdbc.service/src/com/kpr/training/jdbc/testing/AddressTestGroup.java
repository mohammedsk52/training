package com.kpr.training.jdbc.testing;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.service.AddressService;
import com.kpr.training.jdbc.service.ConnectionService;

public class AddressTestGroup {
	
	public Address address;
	public Address addressUpdate;
	
	public CountDownLatch latch;
	
	long createResult;
	long updateResult; 
	Address addressGot;
	List<Address> addressList;
	long deleteResult;
	
	public Address addressRead;
	public Address addressRead1;
	
	public Address nullAddress;
	
	public AddressService addressService = new AddressService();
	
	@BeforeMethod
	public void setUp() {
		
		ConnectionService.get();
		
		address = new Address("35, Goundamani Nagar", "Madurai", 765876);
		
		addressUpdate = new Address("89, Santhanam Avenue", "Thanjavur", 987123);
		
		nullAddress = new Address("null", "null", 0);
		
		latch = new CountDownLatch(1);
		
	}
	
	@Test (priority = 1, description = "Create Address")
	public void createAddress() throws Exception {
		
		try {
			createResult = addressService.create(address);
			
			Assert.assertTrue(createResult > 0);
			
			if(createResult > 0) {
				ConnectionService.commitRollback(true);
			} else {
				ConnectionService.commitRollback(false);
			}
			ConnectionService.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test (priority = 2, description = "Update Address")
	public void updateAddress() throws Exception {
		
		try {
			addressService.update(addressUpdate);
			
			addressRead = addressService.read(addressUpdate.getId());
			
			Assert.assertEquals(addressRead.toString(), addressUpdate.toString());
			
			if(addressRead.toString() == addressUpdate.toString()) {
				ConnectionService.commitRollback(true);
			} else {
				ConnectionService.commitRollback(false);
			}
			
			ConnectionService.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test (priority = 3, description = "Read Address")
	public void readAddress() throws Exception {
		
		try {
			addressGot = addressService.read(42);
			
			Assert.assertEquals(addressGot, address);
		
			if(addressGot.toString() == address.toString()) {
				ConnectionService.commitRollback(true);
			} else {
				ConnectionService.commitRollback(false);
			}
			
			ConnectionService.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test (priority = 4, description = "Read All Address")
	public void readAllAddress() throws Exception{
		try {
			addressList = addressService.readAll();
			
			Assert.assertTrue(addressList != null);
			
			if(addressList != null) {
				ConnectionService.commitRollback(true);
			} else {
				ConnectionService.commitRollback(false);
			}
			
			ConnectionService.release();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	//@Test (priority = 5, description = "Delete Address")
	public void deleteAddress() throws Exception {
		
		try {
			addressService.delete(25);
			
			addressRead1 = addressService.read(25);
			
			Assert.assertEquals(addressRead1.toString(), nullAddress.toString());
			
			if(addressRead1.toString() == nullAddress.toString()) {
				ConnectionService.commitRollback(true);
			} else {
				ConnectionService.commitRollback(false);
			}
			
			ConnectionService.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
