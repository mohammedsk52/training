WBS for AddressService CRUD Operations

Insert Address

/**
 * Problem Statement
 * 1.Perform Insert Address in Address Service
 * 
 * Entity
 * 1.Address
 * 2.InsertAddress
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public long create(Address address);
 * 
 * Jobs to be Done
 * 1.Create address_id of long type and initialize as 0
 * 2.Check whether the address.getPostalCode is equal to 0
 *		2.1)if yes then throw new AppException("402")
 * 3.Create a connection object called connection and call the getConnection method of Connection Class
 * 4.Prepare the preparedStatement called insertAddressStatement and pass the INSERT_ADDRESS_QUERY and return the 
 *   genereted Keys
 * 5.Set the values for the insertAddressStatement 
 * 6.Create addressStatus of long type
 * 7.execute the insertAddressStatement and store the result in addressStatus
 * 8.Create a ResultSet called result and store the generatedKeys from the insertAddressStatement
 * 9.Do while  result has next value
 * 		9.1)Get the logn value from the result and store it in the address_id
 * 10.Check whether the address_id is equal to 0
 * 		10.1)If yes then throw new AppException("403")
 * 11.Close the Connection 
 * 12.return the address_id
 * 
 *
 * Pseudo Code
 * class InsertAddress {
 *	
 *	public long  addressId;
 *	
 *	public long create(Address address) throws Exception {
 *		
 *		long address_id = 0;
 *		
 *		if(address.getPostalCode() == 0) {
 *	 		throw new AppException("402");
 *		}
 *		
 *	 	Connection connection = Connections.getConnection();
 *		
 *		PreparedStatement insertAddressStatement = 
 *				connection.prepareStatement(QueryStatement.INSERT_ADDRESS_QUERY, java.sql.Statement.RETURN_GENERATED_KEYS);
 *		
 *		insertAddressStatement.setString(1, address.getStreet());
 *		insertAddressStatement.setString(2, address.getCity());
 *		insertAddressStatement.setLong(3, address.getPostalCode());
 *		
 *		long addressStatus = insertAddressStatement.executeUpdate();
 *		
 *		ResultSet result = insertAddressStatement.getGeneratedKeys();
 *		while(result.next()) {
 *			address_id = result.getLong(1);
 *		}
 *		
 *		if(address_id == 0) {
 *			throw new AppException("403");
 *		}
 *		connection.close();
 *		
 *		return address_id;
 *		
 *	}
 *
 *}
 *
 */
 
 
 Read Address
 
 
/**
 * Problem Statement
 * 1.Perform Read an Address in AddressService
 * 
 * Requirement
 * 1.Perform Read an Address in AddressService
 * 
 * Entity
 * 1.Address
 * 2.ReadAddress
 * 3.AppException
 * 4.ErrorCode
 *
 * Method Signature
 * 1. public Address readAddress(Person person);
 * 
 * Jobs to be Done
 * 1.Create a Address object called address
 * 2.Check whether person.getId() is equal to 0
 * 		2.1)if yes then throw new AppException("404")
 * 3.Create a Connection object called connection and call the getConnection method of Connections Class
 * 4.Prepare a preparedStatement called statement and call the READ_ADDRESS_QUERY
 * 5.Set the values to the statement
 * 6.Create ResutlSet called result and execute the statement
 * 7.Do while the result has next 
 * 		7.1)Set the value to the address object
 * 8.Check whether the address is equal to null
 * 		8.1)if yes then throw new AppExecption("412")
 * 9.Close the Connection 
 * 10.return address
 * 
 * Pseudo Code
 * 
 * class ReadAddress {
 *	
 *	public Address readAddress(Person person) throws Exception{
 *		
 *		Address address = null;
 *		
 *		try {
 *			if(person.getId() == 0) {
 *				throw new AppException("404");
 *			}
 *			try { 
 *
 *				Connection connection = Connections.getConnection();
 *				
 *				PreparedStatement statement = 
 *						connection.prepareStatement(QueryStatement.READ_ADDRESS_QUERY);
 *				
 *				statement.setLong(1, person.getId());
 *				
 *				ResultSet result = statement.executeQuery();
 *				
 *				while(result.next()) {
 *					address.street = result.getString("street");
 *					address.city = result.getString("city");
 *					address.postal_code = result.getLong("postal_code");
 *				}
 *				
 *				if(address == null) {
 *					throw new AppException("412");
 *				}
 *				connection.close();
 *				
 *			} catch(SQLException e) {
 *				e.printStackTrace();
 *			}
 *		} catch(AppException e) {
 *			e.printStackTrace();
 *		}
 *		
 *		return address;
 *	}
 *
 *}
 */
 
 ReadAllAddress
 
 
/**
 * Problem Statement
 * 1.Perform Read All Address for AddressService
 * 
 * Requirement
 * 1.Perform Read All Address for AddressService
 * 
 * Entity
 * 1.ReadAllAddress
 * 2.Address
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public List<Address> readAllAddress();
 * 
 * 
 * Jobs to be Done
 * 1.Create a List of Address type called addressList
 * 2.Establish a Connection with getConnection method on Connections class
 * 3.Check whether the connection is null
 * 		3.1)throw AppExcepion 401 
 * 4.Prepare the Statement with readAllAddressQuery query from QueryStatement Class
 * 5.execute the Query and store the result to the Result Set called result
 * 6.Do get the statement and store to the address of Address type
 * 		6.1)Add to the List addressList until it does not have next
 * 7.return the addressList
 * 
 * Pseudo Code
 * 
 * class ReadAllAddress { 
 *	
 *	public Address address;
 *	public List<Address> addressList = new ArrayList<>();
 *	
 *	public List<Address> readAllAddress() throws AppException {
 *		
 *		Connection connection = Connections.getConnection();
 *		
 *		if(connection == null) {
 *			throw new AppException("401");
 *		}
 *		
 *		try {
 *			PreparedStatement statement = connection.prepareStatement(QueryStatement.readAllAddressQuery);
 *			
 *			ResultSet result = statement.executeQuery();
 *			
 *			while(result.next()) {
 *				address.street = result.getString("street");
 *				address.city = result.getString("city");
 *				address.postal_code = result.getLong("postal_code");
 *				
 *				addressList.add(address);
 *			}
 *		} catch (SQLException e) {
 *			e.printStackTrace();
 *		}
 *		
 *		return addressList;
 *	} 
 *
 *} 
 *@author UKSD
 */
 
 Update Address
 
 
/**
 * Problem Statement
 * 1.Perform Update Address in Address Service
 * 
 * Entity
 * 1.UpdateAddress
 * 2.Address
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public int update(Address address, long id);
 * 
 * Jobs to be Done
 * 1.Check whether the postalCode is Empty
 * 		1.1)Throw AppException with message "postalCode should not be Empty"
 * 2.Establish Connection using JDBC Driver and store it in connection of Connection type
 * 3.Get the update query from QueryStatement and store it in updateQuery of String type
 * 4.Prepare preparedStatement as statement using connection and return auto generated keys
 *    and updateQuery
 * 5.Set address values to Statement
 * 6.Execute the Statement
 * 7.Store the rows affected in count of int type
 * 8.Check whether the count is 0
 * 		8.1)throw AppException with message "Address updation Failed"
 * 9.return count
 * 
 * Pseudo Code
 * 
 * class UpdateAddress {
 *	
 *	public int count;
 *	
 *	public int update(Address address, long id) throws SQLException {
 *		try {
 *			
 *			if(address.getPostalCode() == 0) {
 *				throw new AppException("402");
 *			}
 *			Connection connection = Connections.getConnection();
 *			
 *			PreparedStatement statement = connection.prepareStatement(QueryStatement.updateAddressQuery);
 *			
 *			statement.setString(1,address.getStreet());
 *			statement.setString(2,address.getCity());
 *			statement.setInt(3,address.getPostalCode());
 *			statement.setLong(4,id);
 *			
 *			count = statement.executeUpdate();
 *			
 *			if(count == 0) {
 *				throw new AppException("403");
 *			}
 *		} catch(AppException e) {
 *			e.printStackTrace();
 *		}
 *		
 *		return count;
 *	} 
 *
 *}
 */
 
 Delete Address
 
 
/**
 * Problem Statement
 * 1.Perform Delete Address for AddressService
 * 
 * Requirement
 * 1.Perform Delete Address for AddressService
 * 
 * Entity
 * 1.Address
 * 2.DeleteAddress
 * 
 * Method Signature
 * 1.public long delete(long id);
 * 
 * Jobs to be Done
 * 1.Create a deletedCount of long type
 * 2.Check whether the id in not equal to 0
 * 		2.1)throw new AppException 404
 * 3.Establish the Connection by getConnection method of Connections Class and store it to connection of Connection type
 * 4.Prepare the preparedStatement with deleteAddressQuery query and store it to statement of preparedStatement
 * 5.Set the id to the statement
 * 6.execute the update and store it to deletedCount
 * 7.Check whether the deletedCount is equal to 0
 * 		7.1) throw new AppException 405
 * 8.return the deletedCount.
 * 
 * Pseudo Code
 * 
 * class DeleteAddress {
 *	
 *	public long deletedCount;
 *	
 *	public long delete(long id) throws AppException {
 *		try {
 *			if(id == 0) {
 *				throw new AppException("404");
 *			}
 *			
 *			Connection connection = Connections.getConnection();
 *			
 *			PreparedStatement statement = connection.prepareStatement(QueryStatement.deleteAddressQuery);
 *			
 *			statement.setLong(1, id);
 *			
 *			deletedCount = statement.executeUpdate();
 *			
 *			if(deletedCount == 0) {
 *				throw new AppException("405");
 *			}
 *		} catch(SQLException e) {
 *			e.printStackTrace();
 *		}
 *		return deletedCount;
 *	}
 *
 *}
 *
 */