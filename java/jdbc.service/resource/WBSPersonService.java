WBS and Pseudo Code for PersonService CRUD Operations

InsertPerson

/**
 * Problem Statement
 * 1.Create a method to create a Person 
 * 
 * Requirement
 * 1.Create a method to create a Person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.public long create(Person person);
 * 
 * Jobs to be Done
 * 1.Create a method called create wtih person as parameter
 * 2.Call the nullChecker and pass the person 
 * 3.Call the personValidator method and pass the person
 * 4.Call the validateEmail method and pass the person
 * 5.Check whether the person address is not equal to null
 * 		5.1)Check whether the postalCode of Address is equal to 0
 * 		5.2)if it is true then throw new AppException(ErrorCodes.E406)
 * 6.Make the preparedStatement and pass the ConnectionService.get method and INSERT_PERSON_QUERY and set 
 *    to try
 * 7.Call the create method from addressService and pass the person address and store to person addressId
 * 8.Set the Values to the preparedStatment with statementValueSetter and pass the preparedStatement and person
 * 9.Create a ResultSet called resultSet
 * 10.Check whether the executeQuery and generatedKeys are equal to 0
 * 		10.1)Rollback the connection 
 * 		10.2)throw new AppExecption(ErrorCode.E407)
 * 11.return the generatedKeys
 * 12.if person address is equal to null
 * 13.Prepare the preparedStatment with ConnectionService.get method and INSERT_PERSON_QUERY in try
 * 14.Set the Values to the preparedStatmenet with statementValueSetter and pass the preparedStatement and person
 * 15.Check whether the ps.executeQuery and generatedKey is equal to 0
 * 		15.1)throw new AppException(ErrorCodes.E406)
 * 16.return the generatedKeys from the resultSet
 * 17.Catch the Exception 
 * 
 * Pseudo Code
 * 
 * public long create(Person person) {
 *
 *		PersonService.nullChecker(person);
 *		
 *		personValidator(person);
 *		
 *		validateEmail(person); 
 *
 *		if (person.getAddress() != null) {
 *			if (person.getAddress().getPostalCode() == 0) {
 *				throw new AppException(ErrorCodes.E406);
 *			}
 *			try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.INSERT_PERSON_QUERY,
 *					Statement.RETURN_GENERATED_KEYS)) {
 *	 			
 *				this.person.getAddress().setId(addressService.create(person.getAddress()));
 *				
 *				statementValueSetter(ps, person);
 *				
 *				ResultSet resultSet; 
 *
 *				if (ps.executeUpdate() == 0 || !(resultSet = ps.getGeneratedKeys()).next()) {
 *					ConnectionService.get().rollback();
 *					throw new AppException(ErrorCodes.E407);
 *				} 
 *				
 *				return resultSet.getLong(Constants.GENERATED_KEY); 
 *
 *			} catch (Exception e) {
 *				throw new AppException(ErrorCodes.E406, e);
 *			}
 *		} else { 
 *
 *			try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.INSERT_PERSON_QUERY,
 *					java.sql.Statement.RETURN_GENERATED_KEYS)) { 
 *
 *				statementValueSetter(ps, person); 
 *
 *				ResultSet resultSet = null;
 *				
 *				if (ps.executeUpdate() == 0 && !(resultSet = ps.getGeneratedKeys()).next()) {
 *					throw new AppException(ErrorCodes.E406);
 *				}
 *				
 *	 			return resultSet.getLong(Constants.GENERATED_KEY); 
 *
 *			} catch (Exception e) {
 *				throw new AppException(ErrorCodes.E406, e);
 *			}
 *			
 *		} 
 *
 *	}
 */

 Read Person
 
 
/**
 * Problem Statement
 * 1.Create a method to read the Person
 * 
 * Requirement
 * 1.Create a method to read the Person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppExecption
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.public Person read(long id, boolean includeAddress);
 * 
 * Jobs to be Done
 * 1.Create a method called read with long id and includeAddress boolean as parameter
 * 2.Create a Person object and initialize as null
 * 3.Check whether the id is equal to 0
 * 		3.1)throw new AppException(ErrorCodes.E404)
 * 4.Prepare the preparedStatement called ps with ConnectionService.get() and READ_PERSON_QUERY in try
 * 5.Set the values to the ps 
 * 6.Execute the Query and store to ResultSet called resultSet
 * 7.return null while the resultSet does not have next item
 * 8.Set the values to the person from resultSet with valueSetter method and store to person
 * 9.Check whether the includeAddress is equal to true
 * 		9.1)read the address with addressService and set to person Address
 * 10.return the person
 * 11.Catch the Exception 
 * 
 * Pseudo Code
 * 
 * public Person read(long id, boolean includeAddress) {
 *
 *		Person person = null; 
 *
 *		if (id == 0) {
 *			throw new AppException(ErrorCodes.E404);
 *		}
 *		
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_PERSON_QUERY)) { 
 *
 *			ps.setLong(1, id); 
 *
 *			ResultSet resultSet = ps.executeQuery(); 
 *
 *			if (!resultSet.next()) {
 *				return null;
 *			} 
 *
 *			person = valueSetter(resultSet); 
 *
 *			if (includeAddress) {
 *				person.setAddress(addressService.read(person.getAddressId()));
 *			}
 *			
 *			return person;
 *			
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E422, e);
 *		}
 *	
 *	}
 */
 
 Read All Person
 
 
/**
 * Problem Statement
 * 1.Create a method to readAll the Person
 * 
 * Requirement
 * 1.Create a method to readAll the Person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.public List<Person> readAll();
 * 
 * Jobs to be Done
 * 1.Create a method called readAll 
 * 2.Create a preparedStatement with ConnectionService.get() and READ_ALL_PERSON_QUERY and execute the Query and store it to resultSet 
 * 		and store to resultSet
 * 3.Create a List of Person type called persons
 * 4.Set the Value from resultSet to person with valueSetter and add the person to persons while the resultSet has next
 * 5.return the persons
 * 6.Catch the Exception
 * 
 * Pseudo Code
 * public List<Person> readAll() {
 *
 *		try (ResultSet resultSet = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_PERSON_QUERY).executeQuery()) { 
 *
 *			List<Person> persons = new ArrayList<>();
 *
 *			while (resultSet.next()) {
 *				persons.add(valueSetter(resultSet));
 *			}
 *			return persons; 
 *
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E423, e);
 *		}
 *	}
 *  
 **/

 
 Update Person
 
 
/**
 * Problem Statement
 * 1.Create a method update with person
 * 
 * Requirement
 * 1.Create a method update with person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.public void update(Person person);
 * 
 * Jobs to be Done
 * 1.Create a method called update with Person in parameter
 * 2.Check whether the person id is equal to 0
 * 		2.1)throw new AppExecption(ErrorCodes.E404);
 * 3.Call the validateEmail and pass the person as parameter and Check whether the email is already present or not 
 * 4.Call the personValidator method and pass the person to Check whether the person is already present or not
 * 5.Prepare the preparedStatement called ps with ConnectionService.get() for connection and UPDATE_PERSON
 * 6.Set the Values with statementValueSetter with ps and person in parameter
 * 7.Check whether the executeUpdate is equal to 0
 * 		7.1)throw new AppException(ErrorCodes.E410)
 * 8.Catch the Exception
 * 
 * Pseudo Code
 * public void update(Person person) {
 *
 *		if (person.getId() == 0) {
 *			throw new AppException(ErrorCodes.E404);
 *		}
 *		
 *		validateEmail(person);
 *		
 *		personValidator(person);
 *		
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_PERSON)) { 
 *
 *			statementValueSetter(ps, person);
 *			
 *			if (ps.executeUpdate() == 0) {
 *				throw new AppException(ErrorCodes.E410);
 *			} 
 *
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E410, e);
 *		}
 *	}
 * 
 */
 
 Delete Person
 
/**
 * Problem Statement
 * 1.To Delete a Person 
 * 
 * Requirement
 * 1.To Delete a Person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.public void delete(long id);
 * 
 * Jobs to be Done
 * 1.Create a method called delete with long id as parameter
 * 2.Check whether the id is equal to 0
 * 		2.1)throw new AppException(ErrorCodes.E404)
 * 3.Prepare the preparedStatement called ps with ConnectionService.get() and DELETE_PERSON in try
 * 4.Set the values to the ps 
 * 5.read the address with read method from the addressService and store to person object
 * 6.Check whether the addressId exists with addressIdChecker
 * 		6.1)If yes then delete the address
 * 7.Execute the Query 
 * 8.Catch the Exception
 * 
 * Pseudo Code
 * public void delete(long id) {
 *
 *		if (id == 0) {
 *			throw new AppException(ErrorCodes.E404);
 *		} 
 *
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.DELETE_PERSON)) { 
 *
 *			ps.setLong(1, id); 
 *
 *			this.person = read(id, false); 
 *
 *			if (addressIdChecker(this.person.getAddressId())) {
 *				addressService.delete(this.person.getAddressId());
 *			} 
 *
 *			ps.executeUpdate(); 
 *
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E409, e);
 *		}
 *	}
 */
 
 /**
 * Problem Statement
 * 1.To Create a method that check addressId exists or not
 * 
 * Requirement
 * 1.To Create a method that check addressId exists or not
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.private boolean addressIdChecker(long addressId);
 * 
 * Jobs to be Done
 * 1.Craete a method called addressIdChecker with one parameter
 * 2.Prepare the preparedStatement called ps and ConnectionService.get() and ADDRESS_IS_CHECKER
 * 3.Set the Values to ps 
 * 4.Check whether the executeQuery and getRow is equal to 1
 * 		4.1)then return false
 * 5.return true
 * 6.Catch the Exception
 * 
 * Pseudo Code
 * private boolean addressIdChecker(long addressId) {
 *
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_ID_CHECKER)) { 
 *
 *			ps.setLong(1, addressId);
 *
 *			if (ps.executeQuery().getRow() == 1) {
 *				return false;
 *			}
 *			return true; 
 *
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E426, e);
 *		}
 *	}
 * 
 */


/**
 * Problem Statement
 * 1.To Validate the Email from the Person
 * 
 * Requirement
 * 1.To Validate the Email from the Person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.public void validateEmail(Person person);
 * 
 * Jobs to be Done
 * 1.Create a method called validateEmail with Person in parameter
 * 2.Prepare the preparedStatement called ps with ConnectionService.get() and EMAIL_CHECKER
 * 3.Set the Values to ps
 * 4.Check whehter the ExecuteQuery has next item 
 * 		4.1) then throw new AppException(ErrorCodes.E412)
 * 5.Catch the Exception
 * 
 * Pseudo Code
 * 
 * public void validateEmail(Person person) {
 *		
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.EMAIL_CHECKER)) { 
 *
 *			ps.setString(1, person.getEmail());
 *			ps.setLong(2, person.getId()); 
 *
 *			if (ps.executeQuery().next()) {
 *				throw new AppException(ErrorCodes.E412);
 *			} 
 *
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E424, e);
 *		} 
 *
 *	}
 */
 
 
 /**
 * Problem Statement
 * 1.To Check the null objects in person
 * 
 * Requirement
 * 1.To Check the null objects in person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.private static void nullChecker(Person person);
 * 
 * Jobs to be Done
 * 1.Check whether the firstName, lastName, and email is equal to null, empty space or empty
 * 		1.1)if yes then throw new AppException(ErrorCodes.E433);
 * 2.Catch the Exception
 * 
 * Pseudo Code
 * 
 * private static void nullChecker(Person person) {
 *		try {
 *			if (person.getFirstName() == null 
 *					|| person.getFirstName() == " " 
 *					|| person.getFirstName() == ""
 *					// lastName
 *					|| person.getLastName() == null 
 *					|| person.getLastName() == " " 
 *					|| person.getLastName() == ""
 *					// Email
 *					|| person.getEmail() == " " 
 *					|| person.getEmail() == "" 
 *					|| person.getEmail() == null
 *					// BirthDate	
 *					|| person.getBirthDate() == null) {
 *						throw new AppException(ErrorCodes.E433);
 *					}
 *		} catch(Exception e) {
 *			throw new AppException(ErrorCodes.E433, e);
 *		}
 *	}
 */

/**
 * Problem Statement
 * 1.To Create Person from CSV File
 * 
 * Requirement
 * 1.To Create Person from CSV File
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public void createPersonWithCSV(String csvfile);
 * 
 * Jobs to be Done
 * 1.Create a method called createPersonWithCSV and parameter as String
 * 2.Create a CSVParser called parser with csvFile in parameter
 * 3.Create a PersonService object called personService
 * 4.For Each record set the values to Person as new object 
 * 		4.1)call the create method from personService and pass the person as parameter
 * 5.Catch the Exception 
 * 
 * Pseudo Code
 * 
 * public void createPersonWithCSV(String csvfile) {
 *
 *		try (CSVParser parser = new CSVParser(new FileReader(csvfile),
 *				CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim())){ 
 *
 *			PersonService personService = new PersonService(); 
 *
 *			for (CSVRecord data : parser) {
 *				Person person = new Person(data.get(Constants.FIRST_NAME),
 *										   data.get(Constants.LAST_NAME),
 *										   data.get(Constants.EMAIL),
 *										   (Date) Validator.dateChanger(data.get(3)), 
 *										   new Address(data.get(Constants.STREET),
 *										   data.get(Constants.CITY),
 *										   Integer.parseInt(data.get(Constants.POSTAL_CODE))));
 *				personService.create(person); 
 *
 *			}
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E406, e);
 *		}
 *	}
 */
 
 /**
 * Problem Statement
 * 1.To validate the Person
 * 
 * Requirement
 * 1.To validate the Person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.private static void personValidator(Person person);
 * 
 * Jobs to be Done
 * 1.Create a method called personValidator with person in parameter
 * 2.Prepare the preparedStatement called ps with ConnectionService.get() and PERSON_CHECKER
 * 3.Set the values to the ps 
 * 4.while(!ps.ExecuteQuery().next())
 * 		4.1)then throw new AppException(ErrorCodes.E433);
 * 5.Catch the Exception 
 * 
 * Pseudo Code
 * 
 * private static void personValidator(Person person) {
 *
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.PERSON_CHECKER)) { 
 *
 *			ps.setString(1, person.getFirstName());
 *			ps.setString(2, person.getLastName()); 
 *
 *			while (!ps.executeQuery().next()) {
 *				throw new AppException(ErrorCodes.E433);
 *			} 
 *
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E433);
 *		}
 *	}
 */
 
 /**
 * Problem Statement
 * 1.To Set the values from resultSet to person
 * 
 * Requirement
 * 1.To Set the values from resultSet to person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.private static Person valueSetter(ResultSet resultSet);
 * 
 * Jobs to be Done
 * 1.return the new person by setting the id, firstName, lastName, email, birthDate, addressId, createdDate
 * to new Person and returning it
 * 2.Catch the Exception
 * 
 * Pseudo Code
 * 
 * private static Person valueSetter(ResultSet resultSet)  {
 *
 *		try {
 *			return new Person(resultSet.getLong(Constants.ID), 
 *					resultSet.getString(Constants.FIRST_NAME),
 *					resultSet.getString(Constants.LAST_NAME), 
 *					resultSet.getString(Constants.EMAIL),
 *					resultSet.getDate(Constants.BIRTH_DATE),
 *					new Address(resultSet.getLong(Constants.ID)),
 *					resultSet.getDate(Constants.CREATED_DATE));
 *		} catch(Exception e) {
 *			throw new AppException(ErrorCodes.E429, e);
 *		}
 *		
 *	}
 */
 
 
 /**
 * Problem Statement
 * 1.To Set the values from preparedStatement to Person 
 * 
 * Requirement
 * 1.To Set the values from preparedStatement to Person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.private static void statementValueSetter(PreparedStatement ps, Person person);
 * 
 * Jobs to be Done
 * 1.Set the values of firstName, lastName, email, birthDate, id from 
 *     preparedStatement to person
 * 2.Catch the Exception
 * 
 * Pseudo Code
 * 
 * private static void statementValueSetter(PreparedStatement ps, Person person) {
 *		try {
 *			ps.setString(1, person.getFirstName());
 *			ps.setString(2, person.getLastName());
 *			ps.setString(3, person.getEmail());
 *			ps.setDate(4, (java.sql.Date) person.getBirthDate());
 *			ps.setLong(5, person.getId());
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E428, e);
 *		}
 *	}
 */
 