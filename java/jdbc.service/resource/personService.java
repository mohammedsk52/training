WBS and Pseudo Code for PersonService CRUD Operations

InsertPerson

/**
 * Problem Statement
 * 1.To Insert Person and Address
 *
 * Requirement
 * 1.To Insert Person and Address

 * Entity
 * 1.PersonService
 * 2.Person
 * 3.Address
 * 4.AppException
 * 5.ErrorCode
 *
 * Method Signature
 * 1.public long create(Person person, Address address);
 *
 * Jobs to be Done
 * 1.Create person_id of type long and initialize as 0
   2.Create address_id of type long and initialize as 0
   3.Create person_idTwo of long type and initialize as 0
   4.Check whether the address is not equal to null
   		4.1)Check whether the person.getName() is equal to null
   		4.2)Check whether the person.getEmail() is equal to null
   		4.3)Check whether the person.getBirthDate() is equal to null
   		4.4)Check whether the person.getPostalCode() is equal to 0
   			4.4.1)If yes then throw new AppException("406")
   5.Create PersonService object as personService
   6.Create AddressService object as addressService
   7.Call the uniqueChecker method of personService and store the result in isUnique
   8.Check whether the isUnique is not equal to 0
   		8.1)throw new AppException("413")
   9.Else Create a Connection object called connection and call the getConnection method
   	  from Connections Class
   10.Prepare the preparedStatement and call the INSERT_PERSON_QUERY and return the generated keys
   11.Call the create method from addressService and store the result in the address_id
   12.Check whether the address_di is not equal to 0
   		12.1)Set the Values to the insertPersonStatement
   		12.2)Execute the insertPersonStatment and store the values to the personStatus
   		12.3)Create a ResultSet called personResult and store the GeneratedKeys from the insertPersonStatement
   		12.4)Do While the personResult has next 
   			12.4.1)Store the personResult value to the person_id
   	    12.5)Check whether the personStatus is equal to 0
   	    	12.5.1)Call the deleteAddress method from the addressService and pass the address_id 
   	    	12.5.2)throw new AppException("407")
   	    12.6)else print Person Creation Success
   13.Close the Connection   
   14.Else the address is equal to null
   15.Check whether the person.getName(), person.getEmail(), person.getBirthDate() is equal to null
   		15.1)throw new AppException("413")
   16.Create a PersonService object called personService
   17.Call the uniqueChecker method from personService and store the value to isUnique
   18.Check whether the isUnique is not equal to 0
   		18.1)throw new AppException("413")
   19.Create a Connection obejct called connectionTwo and call the getConnection method of Connections class
   20.Preapre the preparedStatement called isnertPersonStatementTwo and call the INSERT_PERSON_QUERY and return the generated keys
   21.Set the values to the insertPersonStatementTwo
   22.Execute the insertPersonStatementTwo and store the value to the personStatusTwo of long type
   23.Create a ResultSet called personResultTwo and store the generatedKeys from the insertPersonStatementTwo
   24.Do While the personResultTwo has next 
   		24.1)Store the long value from the personResultTwo to person_idTwo
   25.Check whether the personStatusTwo is not equal to 0
   		25.1)print Person Creation Success
   26.Close the Connection 
   27.return the person_idTwo
   28.if address is provided then return person_id
     
 *
 * Pseudo Code
 *
 * public long create(Person person, Address address) throws Exception{
 *			
 *			long person_id = 0;
 *			long address_id = 0;
 *			long person_idTwo = 0;
 *			
 *			if(address != null) {
 *				 if(person.getName() == null || 
 *					person.getEmail() == null || 
 *					person.getBirthDate() == null || 
 *					address.getPostalCode() == 0) {
 *					 throw new AppException("406");
 *				 }
 *				 
 *				 PersonService personService = new PersonService();
 *				 AddressService addressService = new AddressService();
 *				 long isUnique = personService.uniqueChecker(person);
 *				 if(isUnique != 0) {
 *					 throw new AppException("413");
 *				 } else {
 *					 Connection connection = Connections.getConnection();
 *					 
 *					 try {
 *						PreparedStatement insertPersonStatement = 
 *								connection.prepareStatement(QueryStatement.INSERT_PERSON_QUERY,java.sql.Statement.RETURN_GENERATED_KEYS);
 *						
 *						address_id = addressService.create(address);
 *						
 *						if(address_id != 0) {
 *							insertPersonStatement.setString(1,person.getName());
 *							insertPersonStatement.setString(2,person.getEmail());
 *							insertPersonStatement.setLong(3,address_id);
 *							insertPersonStatement.setDate(4,person.getBirthDate());
 *							
 *							long personStatus = insertPersonStatement.executeUpdate();
 *							
 *							ResultSet personResult = insertPersonStatement.getGeneratedKeys();
 *							while(personResult.next()) {
 *								person_id = personResult.getLong(1);
 *							}
 *							
 *							if(personStatus == 0) {
 *								addressService.deleteAddress(address_id);
 *								throw new AppException("407");
 *							} else {
 *								System.out.println("Person Creation Success");
 *							}
 *						} 
 *						connection.close();
 *					} catch (SQLException e) {
 *						e.printStackTrace();
 *					};
 *				 }
 *			} else {
 *				if(person.getName() == null || 
 *				   person.getEmail() == null || 
 *				   person.getBirthDate() == null) {
 *					throw new AppException("406");
 *				}
 *				PersonService personService = new PersonService();
 *				long isUnique = personService.uniqueChecker(person);
 *				if(isUnique != 0) {
 *					 throw new AppException("413");
 *				} else {
 *					Connection connectionTwo = Connections.getConnection();
 *					try {
 *						PreparedStatement insertPersonStatementTwo = 
 *								connectionTwo.prepareStatement(QueryStatement.INSERT_PERSON_QUERY,java.sql.Statement.RETURN_GENERATED_KEYS);
 *						
 *						insertPersonStatementTwo.setString(1,person.getName());
 *						insertPersonStatementTwo.setString(2,person.getEmail());
 *						insertPersonStatementTwo.setLong(3,1);
 *						insertPersonStatementTwo.setDate(4,person.getBirthDate());
 *						
 *						long personStatusTwo = insertPersonStatementTwo.executeUpdate();
 *						
 *						ResultSet personResultTwo = insertPersonStatementTwo.getGeneratedKeys();
 *						while(personResultTwo.next()) {
 *							person_idTwo = personResultTwo.getLong(1);
 *						}
 *						
 *						if(personStatusTwo != 0) {
 *							System.out.println("Person Creation Success");
 *						}
 *						connectionTwo.close();
 *					} catch(SQLException e) {
 *						e.printStackTrace();
 *					}
 * 			}
 *				return person_idTwo;
 *			}
 *			
 *			return person_id;
 *		}
 **/

 Read Person
 
 
/**
 * Problem Statement
 * 1.Perform Read Person of PersonService
 * 
 * Requirement
 * 1.Perform Read Person of PersonService
 * 
 * Entity
 * 1.Person
 * 2.ReadPerson
 * 3.AppException
 * 4.ErrorCode
 * 
 * Jobs to be Done
 *  1.Create a list of object called objectList
 *  2.Create the Address object called address and initialize it as null
 *  3.Create the Person object called person and initialize it as null
 *  4.Create a Person object called emptyPerson and initialize as null
 *  5.Check whether the id is not equal to 0
 *  		5.1)Create the Connection object called connection and store the result of getConnection method from Connections Class
 *  6.Prepare the preparedStatement for read Person called readPersonStatement
 *  7.Set the valuers for readPersonStatement
 *  8.Store the result of the readPersonStatement in the ResultSet called resultSet
 *  9.Set the values of resultSet to the person object created
 *  10.Check whether the person_id is not equal to 0
 *  		10.1)Add the person object to the objectList
 *  		10.2)Else throw new AppException("408")  
 *  11.Check whether the includeAddress is equal to true
 *  	    11.1)Establish the connection with Connection object as connection and store the result of getConnection of Connection class
 *  		11.2)Prepare the preparedStatement called readAddressStatement
 *  		11.3)Set the values to the readAddressStatement
 *  		11.4)Execute the Query and store the result in the ResultSet called resultSet
 *  		11.5)Set the values of the resultSet to the Address object
 *  		11.6)Check whether the address equal to null
 *  			11.6.1)throw new AppException("412")
 *  			11.6.2)Else add the address object to the objectList 
 * 	12.Return the objectList.
 * 
 * 
 * Pseudo Code
 * 
 *  class ReadPerson {
 *	
 *	public Person person;
 *	
 *		public List<Object> readPerson(Person personGot, boolean includeAddress) throws Exception {
 *		
 *		List<Object> objectList = new ArrayList<Object>();
 *		Address address = new Address(null, null, personGot.getId());
 *		Person person = new Person(null, null, null);
 *		Person emptyPerson = new Person(null, null, null);
 *		
 *		if(personGot.getId() != 0) {
 *			Connection connection = ConnectionService.initConnection();
 *			
 *			try {
 *				PreparedStatement readPersonStatement = 
 *						connection.prepareStatement(QueryStatement.READ_PERSON_QUERY, Statement.RETURN_GENERATED_KEYS);
 *				
 *				readPersonStatement.setLong(1,personGot.getId());
 *				
 *				ResultSet result = readPersonStatement.executeQuery();
 *				
 *				while(result.next()) {
 *					person.setId(result.getLong("id"));
 *					person.setName(result.getString("name"));
 *					person.setEmail(result.getString("email"));
 *					person.setAddressId(result.getLong("address_id"));
 *					person.setCreatedDate(result.getDate("birth_date"));
 *					person.setBirthDate(result.getDate("created_date"));
 *				}
 *				
 *				ConnectionService.releaseConnection(connection);
 *				
 *			} catch(SQLException e) {
 *				e.printStackTrace();
 *			}
 *			
 *			if(person.id != 0) {
 *				objectList.add(person);
 *			} else {
 *				objectList.add(emptyPerson);
 *			}
 *			
 *			if(includeAddress == true) {
 *				AddressService addressService = new AddressService();
 *				
 *				address = addressService.readAddress(person);
 *				
 *				if(address == null) {
 *					throw new AppException(ErrorCodes.ADDRESS_NOT_FOUND);
 *				}
 *				
 *				objectList.add(address);
 *			}
 *		}
 *		return objectList;
 *		//return Arrays.asList(person, address);
 *	}
 * @author UKSD
 *
 */
 
 Read All Person
 
 
/**
 * Problem Statement
 * 1.Perform Read All Person for Person Service
 * 
 * Requirement
 * 1.Perform Read All Person for Person Service
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public List<Person> readAllPerson();
 *
 * Jobs to be Done
 * 1.Create a Person list called allPerson
 * 2.Create a person of Person type
 * 3.Establish the Connection with getConnection method from the Connections Class
 * 4.Check whether the connection is equal to null
 * 		4.1)throw AppException("401")
 * 5.Prepare the PreparedStatmenet called statement with the readAllPersonQuery.
 * 6. execute the Query and Store the result in resultSet called result
 * 7.Check while the the result has next 
 * 		7.1)Store the id to person.id
 * 		7.2)Store the name to person.name
 * 		7.3)Store the email to person.email
 * 		7.4)Store the address_id to person.address_id
 * 		7.5)Store the birth_date to person.birthdate
 * 		7.6)Store the created_date to person.created_date
 * 
 * 8.Add the person to addPerson List
 * 9.Check whether the addPerson is equal to null
 * 		9.1)throw AppException("408")
 * 10.return allPerson
 * 
 * 
 * Pseudo Code
 * 
 * class ReadAllPerson {
 *	
 *	List<Person> allPerson = new ArrayList<>();
 *	
 *	Person person;
 *	
 *	public List<Person> readAllPerson() throws AppException {
 *		
 *		Connection connection = Connections.getConnection();
 *		
 *		if(connection == null) {
 *			throw new AppException("401");
 *		}
 *		
 *		try {
 *			PreparedStatement statement = connection.prepareStatement(QueryStatement.readAllPersonQuery);
 *			
 *			ResultSet result = statement.executeQuery();
 *			
 *			while(result.next()) {
 *				person.id = result.getLong("id");
 *				person.name = result.getString("name");
 *				person.email = result.getString("email");
 *				person.address_id = result.getLong("address_id");
 *				person.birth_date = result.getDate("birth_date");
 *				person.created_date = result.getDate("created_date");
 *				
 *				allPerson.add(person);
 *			}
 *		} catch (SQLException e) {
 *			e.printStackTrace();
 *		}
 *		
 *		if(allPerson == null) {
 *			throw new AppException("408");
 *		}
 *		return allPerson;
 *	} 
 *
 *}
 * 
 */
 
 Update Person
 
 
/**
 * Problem Statement
 * 1.Perform Update Person in PersonService
 * 
 * Requirement
 * 1.Perform Update Person in PersonService
 * 
 * Entity
 * 1.UpdatePerson
 * 
 * Method Signature
 * 1.public long update(Person person);
 * 
 * Jobs to be Done
 * 1.Check whether the person id is equal to 0
 * 		1.1)throw new AppException("409")
 * 2.Establish the Connection with getConnection of Connections Class
 * 3.Check Whether the connection is null
 * 		3.1)throw new AppException("401")
 * 4.Prepare the statement with connection with QueryStatement of updatePerson
 * 5.Set the Statement values
 * 6.Execute the statment and store the result in changes
 * 7.Check Whether the changes is equal to 0
 * 		7.1)throw new AppException("411")
 * 8.return changes
 * 
 * 
 * Pseudo Code
 * 
 * class UpdatePerson {
 *	
 *	public int changes;
 *	
 *	public long update(Person person) throws AppException {
 *		
 *		if(person.getId() == 0) {
 *			throw new AppException("409");
 *		}
 *		
 *		Connection connection = Connections.getConnection();
 *		
 *		if(connection == null) {
 *			throw new AppException("401");
 *		}
 *		
 *		try {
 *			PreparedStatement statement = connection.prepareStatement(QueryStatement.updatePerson);
 *			
 *			statement.setString(1,person.getName());
 *			statement.setString(1,person.getEmail());
 *			statement.setDate(1,person.getBirthDate());
 *			statement.setDate(1,person.getCreatedDate());
 *			statement.setLong(1,person.getId());
 *  			
 *			changes = statement.executeUpdate();
 *		} catch (SQLException e) {
 *			e.printStackTrace();
 *		}
 *		
 *		if(changes == 0) {
 *			throw new AppException("411");
 *		}
 *		
 *		return changes;
 *	} 
 *
 *}
 */
 
 Delete Person
 
/**
 * Problem Statement
 * 1.Delete Person Address Cascade 
 *
 * Requirement
 * 1.Delete Person Address Cascade
 *
 * Entity
 * 1.Person
 * 2.Address
 * 3.PersonService
 * 4.AppException
 * 5.ErrorCode
 *
 * Method Signature
 * 1.public long delete(person);
 *
 * Jobs to be Done
 * 1.Check whether the id is equal to 0
 *   1.1)throw new AppException("Id cannot be 0")
 * 2.Establish the Connection with getConnection method in Connections class and store it in connection
 *   of Connection type
 * 3.Prepare the prearedStatement personStatement with deletePerson query from QueryStatmenet and return AUTO_GENERATED_KEYS
 * 4.Prepare the prearedStatement addressStatement with deleteAddress query from QueryStatmenet
 * 5.Set the value to personStatement
 * 6.executeUpdate the personStatement and store the result in deleteStatus of long type 
 * 7.Store the generated keys in address_id
 * 8.Check the deletePersonStatus is equal to 0.
 *     8.1)throw new AppException("Person Deletion Failed");
 * 9.set the value to the addressStatement 
 * 10.execute the addressStatement and store the result in deleteAddressStatus
 * 11.Check whether the deleteAddressStatus is equal to 0
 *   11.1)throw new AppException("Address Deletion Failed")
 * 12.print Person Deletion Success
 * 13.return deletePersonStatus
 * 14.Close the Connections
 *
 * Pseudo Code
 *
 * class PersonService {
 *   
 *   public int delete(long id) {
 *       
 *       if(id == 0) {
 *           throw new AppException("404")
 *       }
 *       Connection connection = Connections.getConnection();
 *       
 *       PreparedStatement personStatement = 
 *           connection.preparedStatement(QueryStatement.deletePerson, statememt.AUTO_GENERATED_KEYS);
 *       PreparedStatement addressStatement = 
 *           connection.preparedStatement(QueryStatement.deleteAddress);
 *       
 *       personStatement.setLong(1, id);
 *       
 *       long deleteStatus = personStatement.executeUpdate();
 *       
 *       ResultSet result = personStatement.getGeneratedKeys();
 *       while(result.next()) {
 *           long address_id = result.getLong(1);
 *       }
 *       if(deleteStatus == 0) {
 *           throw new Exception("Person Deletion Failed")
 *       }
 *        addressStatement.setLong(1, address_id);
 *       int deleteAddressStatus = addressStatement.executeUpdate();
 *       
 *       if(deleteAddressStatus == 0){
 *           throw new AppException("Address Deletion Failed");
 *       } else {
 *           System.out.println("Person Deletion Success");
 *       }
 *       return deleteStatus;
 *   }
 *}
 *
 *
 *
 * @author UKSD
 */