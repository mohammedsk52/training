WBS for AddressService CRUD Operations

Insert Address

/**
 * Problem Statement
 * 1.Perform Insert Address in Address Service
 *
 * Requirement
 * 1.Perform Insert Address in Address Service
 * 
 * Entity
 * 1.Address
 * 2.AddressService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public long create(Address address);
 * 
 * Jobs to be Done
 * 1.Call the addressValidator method from AddressService and pass the address
 * 2.Pass the preparedStatement with Connection.get method from ConnectionService and pass
 *   the INSERT_ADDRESS_QUERY 
 * 3.Call the addressChecker and pass the address and connection and store it in existAddressId of long type
 * 4.Check whether the existAddressId is not equal to 0
 * 		4.1) if not equal then return existAddressId
 * 5.Call the statementValueSetterAddress and pass the preparedStatement and address
 * 6.execute the preparedStatement
 * 7.Store the generatedKey from prepareStatement and store to resultSet
 * 8.Check while the resultset is empty
 * 		8.1)throw new AppExecption(ErrorCodes.E406)
 * 9.return generatedKeys
 *  
 *
 * Pseudo Code
 * class InsertAddress {
 *	
 *	public long create(Address address){
 *		
 *		addressValidator(address); 
 *
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.INSERT_ADDRESS_QUERY,
 *				java.sql.Statement.RETURN_GENERATED_KEYS)) {
 *			
 *			long existAddressId = addressChecker(address, ConnectionService.get()); 
 *
 *			if (existAddressId != 0) {
 *				return existAddressId;
 *			} 
 *
 *			statementValueSetterAddress(ps, address);
 *			ps.executeUpdate(); 
 *
 *			ResultSet resultSet = ps.getGeneratedKeys(); 
 *
 *			while (!resultSet.next()) {
 *				throw new AppException(ErrorCodes.E406);
 *			}
 *			return resultSet.getLong(Constants.GENERATED_KEY); 
 *
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E403,e);
 *		}
 *	}
 *
 */
 
 
 Read Address
 
 
/**
 * Problem Statement
 * 1.Perform Read an Address in AddressService
 * 
 * Requirement
 * 1.Perform Read an Address in AddressService
 * 
 * Entity
 * 1.Address
 * 2.AddressService
 * 3.AppException
 * 4.ErrorCode
 *
 * Method Signature
 * 1. public Address read(long addressId);
 * 
 * Jobs to be Done
 * * 1.Create a method called with long
 * 2.Create a preparedStatement and call the ConnectionService.get and with READ_ADDRESS_QUERY 
 *    inside try
 * 3.Check whether the addressId is equal to 0
 * 		3.1)if yes then throw new AppException(ErrorCodes.E404)
 * 4.Set the values to preparedStatement
 * 5.Execute the preparedStatement and store to resultSet
 * 6.Do return null if the resultSet is empty
 * 7.return valueSetter method with resultSet as paramter
 * 
 * Pseudo Code
 * 
 * class ReadAddress {
 *	
 *	public Address read(long addressId) {
 *
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_ADDRESS_QUERY)) { 
 *
 *			if (addressId == 0) {
 *	 			throw new AppException(ErrorCodes.E404);
 *			} 
 *
 *			ps.setLong(1, addressId); 
 *
 *			ResultSet resultSet = ps.executeQuery(); 
 *
 *			if(!resultSet.next()) {
 *				return null; 
 *			}
 *			
 *			return valueSetter(resultSet); 
 *
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E425, e);
 *		}
 *		
 *	}
 */
 
 ReadAllAddress
 
 
/**
 * Problem Statement
 * 1.Perform Read All Address for AddressService
 * 
 * Requirement
 * 1.Perform Read All Address for AddressService
 * 
 * Entity
 * 1.AddressService
 * 2.Address
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public List<Address> readAll();
 * 
 * Jobs to be Done
 * 1.Create the method called readAll 
 * 2.Call the connectionService.get and pass to preparedStatement and READ_ALL_ADDRESS_QUERY
 * 3.Create a List of type Address called addresses
 * 4.Call the valueSetter with resultSet and add to addresses while the resultSet has next
 * 5.return addresses
 * 
 * Pseudo Code
 * 
 * class ReadAllAddress { 
 *	
 *	public List<Address> readAll() {
 *		
 *		try (ResultSet resultSet = ConnectionService.get()
 *				.prepareStatement(QueryStatement.READ_ALL_ADDRESS_QUERY)
 *				.executeQuery()) { 
 *
 *			List<Address> addresses = new ArrayList<>(); 
 *
 *			while (resultSet.next()) {
 *				addresses.add(valueSetter(resultSet));
 *			}
 *	 		
 *			return addresses;
 *			
 *	 	} catch (SQLException e) {
 *			throw new AppException(ErrorCodes.E425,e);
 *		}
 *		
 *	}
 *@author UKSD
 */
 
 Update Address
 
 
/**
 * Problem Statement
 * 1.Perform Update Address in Address Service
 *
 * Requirement
 * 1.Perform Update Address in Address Service
 * 
 * Entity
 * 1.AddressService
 * 2.Address
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public void update(Address address);
 * 
 * Jobs to be Done
 * 1.Create a method called update with address in parameter
 * 2.Pass the preparedStatement with ConnectionService.get and UPDATE_ADDRESS_QUERY
 * 3.Set the values to preparedStatement
 * 4.Execute the Query
 * 5.throw AppException(ErrorCodes.E409) when preparedStatement execution is equal to 0
 * 
 * 
 * Pseudo Code
 * 
 * class UpdateAddress {
 *	
 *	public void update(Address address) {
 *		
 *		addressValidator(address);
 *		
 *		try (PreparedStatement ps = ConnectionService.get()
 *				.prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY)) {
 *			
 *			statementValueSetterAddress(ps, address);
 *			ps.setLong(4, address.getId()); 
 *
 *			if (ps.executeUpdate() == 0) {
 *				throw new AppException(ErrorCodes.E403);
 *			}
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E405,e);
 *		}
 *	}
 */
 
 Delete Address
 
 
/**
 * Problem Statement
 * 1.Perform Delete Address for AddressService
 * 
 * Requirement
 * 1.Perform Delete Address for AddressService
 * 
 * Entity
 * 1.Address
 * 2.AddressService
 * 3.QueryStatement
 * 4.ErrorCodes
 * 5.AppException
 * 
 * Method Signature
 * 1.public void delete(long id);
 * 
 * Jobs to be Done
 * 1.Create a method called delete with id in paramter
 * 2.Pass the preparedStatement with ConnectionService.get and DELETE_ADDRESS_QUERY
 * 3.Set the values to preparedStatement
 * 4.Execute the query and Check whether the executeQuery is equal to 0 
 * 		4.1)if yes then throw new AppException(ErrorCodes.E405)
 * 
 * 
 * Pseudo Code
 * 
 * class DeleteAddress {
 *	
 *	public void delete(long id) {
 *
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY)) { 
 *
 *			ps.setLong(1, id); 
 *
 *			if (ps.executeUpdate() == 0) {
 *				throw new AppException(ErrorCodes.E405);
 *			}
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E431, e);
 *		}
 *	}
 *
 *}
 *
 */
 
 
/**
 * Problem Statement
 * 1.Search Address for AddressService
 *
 * Requirement
 * 1.Search Address for AddressService
 * 
 * Entity
 * 1.AddressService
 * 2.AppException
 * 3.ErrorCodes
 * 4.Address
 * 
 * Method Signature
 * 1.public List<Address> search(Address address);
 * 
 * Jobs to be Done
 * 1.Create a method called search with address in parameter
 * 2.Call the addressValidator with address in parameter
 * 3.Pass the preparedStatement with connection and SEARCH_QUERY in try
 * 4.Call the statementVAlueSetterAddress with preparedStatement and address
 * 5.Execute the preparedStatement and store to resultSet
 * 6.Create the List of address type called addresses
 * 7.Set the values from the resultSet and add the address to the 
 *    addresses list while the resultSet has next item
 * 8.return addresses
 * 9.Catch the Exception
 * 
 * 
 * Pseudo Code
 *  
 * public List<Address> search(Address address) {
 *
 *		addressValidator(address); 
 *
 *		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.SEARCH_QUERY)) { 
 *
 *			statementValueSetterAddress(ps, address); 
 *
 *			ResultSet resultSet = ps.executeQuery();
 *			
 *			List<Address> addresses = new ArrayList<>();
 *			
 *			while(resultSet.next()) {
 *				this.address = valueSetter(resultSet);
 *				this.address.setId(resultSet.getLong(Constants.ID));
 *	 			addresses.add(this.address);
 *			}
 *			
 *			return addresses;
 *			
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E432, e);
 *		}
 *		
 *	}
 */
 
 
 /**
 * Problem Statement
 * 1.Create a method to check the address exists or not
 *
 * Requirement
 * 1.Create a method to check the address exists or not
 * 
 * Entity
 * 1.Address
 * 2.AddressService
 * 3.AppException
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.public long addressChecker(Address address, Connection connection);
 * 
 * Jobs to be Done
 * 1.Create the addressChecker method with address and connection paramter
 * 2.Pass the preparedStatement with connection and ADDRESS_CHECK
 * 3.set the values to preparedStatement and address
 * 4.Execute the query and store to resultSet
 * 5.return 0 while the resultSet has no next item
 * 6.return the addressId
 * 7.Catch the Exception
 * 
 * Pseudo Code
 * public long addressChecker(Address address, Connection connection) {
 *		
 *		try (PreparedStatement ps = connection.prepareStatement(QueryStatement.ADDRESS_CHECK)) { 
 *
 *			//new method
 *			statementValueSetterAddress(ps, address); 
 *
 *			ResultSet resultSet = ps.executeQuery(); 
 *
 *			while (!resultSet.next()) {
 *				return 0;
 *			}
 *			
 *			return resultSet.getLong(Constants.ID);
 *			
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E417, e);
 *		}
 *		
 *	}
 */
 
 /**
 * Problem Statement
 * 1.Create a method to validate the address
 *
 * Requirement
 * 1.Create a method to validate the address
 * 
 * Entity
 * 1.Address
 * 2.AddressService
 * 3.AppException
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.private void addressValidator(Address address)
 * 
 * Jobs to be Done
 * 1.Create a method called addressValidator with address as parameter
 * 2.Check whether the address postalCode, street, city is equal to null
 * 		and street, city is equal to empty and space
 * 		2.1)throw new AppException(ErrorCodes.E430)
 * 
 * Pseudo Code
 * private void addressValidator(Address address) {
 *		if (address.getPostalCode() == 0 
 *				|| address.getStreet() == null //trim()
 *				|| address.getCity() == null
 *			    || address.getCity() == " "
 *				|| address.getCity() == ""
 *				|| address.getStreet() == " "
 *				|| address.getStreet() == "" ) {
 *			throw new AppException(ErrorCodes.E430);
 *		} 
 *	}
 */
 
 
 /**
 * Problem Statement
 * 1.Create method to Set the values to resultSet
 *
 * Requirement
 * 1.Create method to Set the values to resultSet
 * 
 * Entity
 * 1.Address
 * 2.AddressService
 * 3.AppException
 * 4.ErrorCodes
 * 
 * Method Signature
 * 1.private Address valueSetter(ResultSet resultSet)
 * 
 * Jobs to be Done
 * 1.return a new Address by setting the values to address from resultSet
 * 2.Catch the Exception
 * 
 * Pseudo Code
 * private Address valueSetter(ResultSet resultSet) {
 *		try {
 *			return new Address(resultSet.getString(Constants.STREET)
 *					   ,resultSet.getString(Constants.CITY)
 *					   ,resultSet.getLong(Constants.POSTAL_CODE));
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E429, e); 
 *		}
 *		
 *	} 
 * 
*/

/**
 * Problem Statement
 * 1.Create a method to values to preparedStatement from address
 *
 * Requirement
 * 1.Create a method to values to preparedStatement from address
 * 
 * Entity
 * 1.Address
 * 2.AddressService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.private void statementValueSetterAddress(PreparedStatement ps, Address address);
 * 
 * Jobs to be Done
 * 1.Set the values to the preparedStatement from Address
 * 2.Catch the Exception
 * 
 * Pseudo Code
 * private void statementValueSetterAddress(PreparedStatement ps, Address address) {
 *		try {
 *			ps.setString(1, address.getStreet());
 *			ps.setString(2, address.getCity());
 *			ps.setLong(3, address.getPostalCode());
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E428, e);
 *	 	}
 *	}
 */
 
 