/* Requirement:
 *     write a program to print difference of two numbers using lambda expression 
 *     and the single method interface
 * Entity:
 *     LambdaInterfaceDifference
 *     Difference
 * Function Declaration:
 *     public static void main(String[] args)
 *     public int difference(int number1 , int number2);
 * Jobs To Be Done:
 *     1)create interface method named difference with two parameters.
 *     2)pass the parameters to difference method of the interface.
 *     3)returns the difference between two inputs.
 *     4)print the difference between two inputs.
 *     
 *pseudo code:
 *		interface LambdaInterfaceConcat{

                 String concat(String string1 , String string2);
         }

         public class ConcatString {
	            //returns the (number1- number2) using lambda operation.
	              
	            public static void main(String[] args) {	
			             System.out.println(lambdaInterfaceDifference.difference(10,5));	        
	            }
         }
 *
 */
package com.training.java.exercise3.lambdaexpressions;

interface LambdaInterfaceDifference {
	
	      public int difference(int number1 , int number2);
}

public class Difference {
	
	static LambdaInterfaceDifference lambdaInterfaceDifference = (number1 , number2) -> { 
	    return number1 - number2 ;
    } ;
	
public static void main(String[] args) {		
		System.out.println(lambdaInterfaceDifference.difference(10,8));
	}
}
