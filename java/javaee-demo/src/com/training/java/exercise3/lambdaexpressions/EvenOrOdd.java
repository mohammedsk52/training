/*
 * Requirements : 
 *       To convert the following anonymous class into lambda expression.
 *  interface CheckNumber {
        public boolean isEven(int value);
    }
    
    public class EvenOrOdd {
       
        public static void main(String[] args) {
            CheckNumber number = new CheckNumber() {
                public boolean isEven(int value) {
                    if (value % 2 == 0) {
                        return true;
                    } else return false;
                }
            };
            System.out.println(number.isEven(11));
        }
    }
 * Entities :interface CheckNumber,
 *           public class EvenOrOdd.
 * Function Declaration :boolean isEven(int number);
 *                       public static void main(String[] args).
 * Jobs To Be Done:1)Consider the program
 *       		   2)Creating a CheckNumber interface and create a isEven method.
 *      		   3)Inside the main method
 *      		   4)Creating a object for the Scanner named scanner
 *                 5)A variable is declared of type integer named num.
 *                 6)The user is prompted to enter the value which will be stored in num.
 *     			   5)Creating check reference for CheckNUmber interface and define a lambda function.
 *     			   6)Printing the result as true if number is even or it will print as false
 *                   using isEven method.
 *              
 */
package com.training.java.exercise3.lambdaexpressions;


import java.util.Scanner;

interface CheckNumber {
    
    boolean isEven(int number);
    
}

public class EvenOrOdd {

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number:");
        int num = scanner.nextInt();
        CheckNumber check = (int number) -> {
        	return (number % 2 == 0) ;
        };
        System.out.println(check.isEven(num));
        scanner.close();
    }
}