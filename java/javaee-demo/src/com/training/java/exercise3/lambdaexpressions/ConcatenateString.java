/* Requirement:
 *     write a Lambda expression program with a single method interface
 *     To concatenate two strings.
 * Entity:
 *     LambdaInterfaceConcat
 *     ConcatString
 * Function Declaration:
 *     public static void main(String[] args)
 *     public String concatString(String string1 , String string2);
 * Jobs To Be Done:
 * 	   1)create interface method named concat with two parameters.
 *     2)pass the parameters to concat method of the interface.
 *     3)returns the concatenated string.
 *     4)print the returned string.
 *     
 *pseudo code:
 *		interface LambdaInterfaceConcat{

                 String concat(String string1 , String string2);
         }

         public class ConcatString {
	            //returns the (string1 + string2) using lambda expression.
	              
	            public static void main(String[] args) {	
			             System.out.println(lambdaInterfaceConcat.concat("BALL ", "BADMINTON"));	        
	            }
         }
 *
 */

package com.training.java.exercise3.lambdaexpressions;

interface LambdaInterfaceConcat {

    String concat(String string1 , String string2);
}

public class ConcatenateString {
	
	static LambdaInterfaceConcat lambdaInterfaceConcat = (string1, string2) -> {
		return string1 + string2 ;
	};
	
	 public static void main(String[] args) {	
			System.out.println(lambdaInterfaceConcat.concat("BALL ", "BADMINTON"));	        
	    }
}
