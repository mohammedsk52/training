/*
Requirement:
    To find the difference between the times using timer class.
    
Entity:
    TimeDifference
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Declare a variable named startTime of type long.
    	1.1) invoke the method System.currentTimeMillis(), which gets the Starting time in milliseconds.
        1.2) Print that starting time.
    2. using for loop print upto 100 from 0.
    3. Now, Declare a variable named endTime of type long.
    	1.1) invoke the method System.currentTimeMillis(), which gets the ending time in milliseconds.
        1.2) Print that ending time.
    3. print the difference between end time and the start time in ms.
    
Pseudo code:

class TimeDifferenceDemo {
    
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        //print the starttime
         *         
        for (int time = 0; time < 100 ; time++) {
            System.out.println(time);
        }
        
        long endTime = System.currentTimeMillis();
        //print the end time
        System.out.println("THe difference between the time is: " + (startTime - endTime) + "  " + "ms");
    }
}
*/

package com.training.java.exercise3.dateandtime;

public class TimeDifference {

	public static void main(String[] args) {
		long start = System.currentTimeMillis(); 
		System.out.println(  "staring time in millsecond  :  " + start + "ms");

		for (long time = 0; time < 100; time++) {
			System.out.println(time);
		}

		long end = System.currentTimeMillis(); 
		System.out.println("End  time in millsecond :  " + end + "ms");
		System.out.println("Counting to 100 takes " + (end - start) + "ms"); 
	} 
}
