/*Requirement:* Write a Java program to copy all of the mappings from the specified map to another map?
              * Write a Java program to test if a map contains a mapping for the specified key?
              * Count the size of mappings in a map
 Entity: MapDemo
 Function Declaration:public static void main(String[] args)
 Jobs To Be Done:1.consider the program
                 2.Two maps created named hash_map1 and hash_map2
                 3.The values are inserted into hash_map1
                 4.Then all the elements of hash_map1 is inserted to hash_map2 
                   using putAll
                 5.Then containsKey is used check whether the key is present
                 6.if the condition is satisfied "yes key is present" will be printed.
                 7.otherwise "not present" will be printed.
                 8.Then the size of the map is printed using size method.             
 */

package com.training.java.exercise3.map;

import java.util.HashMap;

public class MapDemo {
	
	public static void main(String[] args) {
		
		  HashMap <Integer,Integer> hash_map1 = new HashMap <Integer,Integer> ();
		  HashMap <Integer,Integer> hash_map2 = new HashMap <Integer,Integer> ();
		  hash_map1.put(1,10);
		  hash_map1.put(2,20);
		  hash_map1.put(3, 30);
		  System.out.println("first map values: " + hash_map1);
		  
		  //copy first map to second map
		  hash_map2.putAll(hash_map1);
		  System.out.println("After copying values of first map to second map: " + hash_map2);
		  
		  //to check key is present or not
		  if (hash_map1.containsKey(10)) {
			  System.out.println("Yes key is present");
		  } else {
			  System.out.println("key is not  present");
		  }
		  
		  //count the size of the map
		  System.out.println("Size of the hash map: "+hash_map1.size());	  
		
	}

}
