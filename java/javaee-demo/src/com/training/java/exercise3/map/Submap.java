/*Requirement:To write a Java program to get the portion of a map whose keys 
 *            range from a given key to another key
 *Entity:Submap
 *Function Declaration:public static void main(String[] args)
 *Jobs To Be Done:1.consider the program
 *                2.A TreeMap is created named tree_map
 *                3.The values are inserted into the tree_map using put
 *                4.tree_map is printed.
 *                5.The Submap is printed from  key 3 to 5
 *                
 */
package com.training.java.exercise3.map;

import java.util.TreeMap;

public class Submap {


	public static void main(String[] args) {
		
		TreeMap<Integer, Integer> tree_map = new TreeMap<Integer, Integer>(); 
		tree_map.put(1 , 10); 
		tree_map.put(2 , 20); 
		tree_map.put(3 , 30); 
		tree_map.put(4 , 40);
		tree_map.put(5 , 50); 

    
		System.out.println("Map:"+ tree_map); 

		System.out.println("Submap:" +  tree_map.subMap(3,5)); 
		
	}
}
