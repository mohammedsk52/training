/*Requirement:
 *      To Write a program to filter the Person, who are male and age greater than 21
 * 
 * Entity:
 *      GenderAgePerson
 * 
 * Function signature:
 *      public static void main(String[] args)
 * 
 * Jobs To Be Done:
 *      1) Create the List with reference to precreated method in person.java file.
 *      2) Check the each person's gender is equal to Male.
 *          2.1) if the person is male then check the age age is greater than 21.
 *              2.1.1) if the person age is greater than 21, print the person name.
 *       
 * Pseudocode:
 * 
 * public class GenderAgePerson {
 * 
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
 *      for (Person p : roster) {
 *          if(gender==male){
 *              if(age>21){
 *                    System.out.println(name);
 *              }
 *          }
 *     }
 *   }
 *}              
 */

package com.training.java.exercise3.stream;

import java.util.List;

public class GenderAgePerson {
    
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        for (Person p : roster) {
            if (p.getGender() == Person.Sex.MALE) {
                if (p.getAge() > 21) {
                    System.out.println(p.getName());
                }
            }

        }
    }
}