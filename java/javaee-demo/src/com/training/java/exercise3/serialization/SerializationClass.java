/*
Create a File, get the Following fields as input from the user 
    1.Name
    2.studentId
    3.Address
    4.Phone Number
now store the input in the File and serialize it, and again de serialize the File and print the content.


1.Requirements:
     Program to store the input in the File and serialize it, and again de serialize the File and print the 
content.

2.Entities:
      SerializationDemo

3.Jobs to be done:
    1.Get an input from the user for name, stuentid, address, phonenumber.
    2.Try block
      2.1)Set the path to store ser file and store it in fileout.
      2.2)Create an object for OutputStream name out for fileOut.
      2.3)Write in using writeObject method and close ObjectOutputStream writeFile object.
    3.Catch IOException invoke printStackTrace method. 
 
pseudo code:-
import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;     
    public class SerializationClass {
	
	public static void main(String[]args) {
		Student student = new Student();
		student.StudentName = "Taha";
		student.StudentId = "18cs063";
		student.StudentAddress = "coimbatore";
		student.StudentPhoneNumber = "9384784053";
		
		try {
			
			FileOutputStream fileOut = new FileOutputStream("C:\\DEV\\training1\\java\\javaee-demo\\src\\com\\training\\java\\exercise3\\serialization\\StudentDetails.txt");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(student);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in StudentDetails.txt file");
		}
		
		catch(IOException e) {
			
			e.printStackTrace();
		}
	}
}
	

  */
package com.training.java.exercise3.serialization;
import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

public class SerializationClass {
	
	public static void main(String[]args) {
		Student student = new Student();
		student.StudentName = "Taha";
		student.StudentId = "18cs063";
		student.StudentAddress = "coimbatore";
		student.StudentPhoneNumber = "9384784053";
		
		try {
			
			FileOutputStream fileOut = new FileOutputStream("C:\\DEV\\training1\\java\\javaee-demo\\src\\com\\training\\java\\exercise3\\serialization\\StudentDetails.txt");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(student);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in StudentDetails.txt file");
		}
		
		catch(IOException e) {
			
			e.printStackTrace();
		}
	}
}
	
