package com.training.java.exercise3.serialization;
import java.io.*;

public class DeseriaizationClass
{
	public static void main (String[] args) throws IOException,ClassNotFoundException {
		Student student = null;
		try{
			
			FileInputStream fileIn = new FileInputStream("C:\\DEV\\training1\\java\\javaee-demo\\src\\com\\training\\java\\exercise3\\serialization\\StudentDetails.txt");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			student = (Student) in.readObject();
			in.close();
			fileIn.close();
		}
		finally{
			System.out.println("Deserializing Student.......");
			System.out.println("Name Of the Student :" + student.StudentName);
			System.out.println("Id Of the Student :" + student.StudentId);
			System.out.println("Address Of the Student :" + student.StudentAddress);
			System.out.println("Phone Number Of the Student :" + student.StudentPhoneNumber);
		}
	}
}