/** Requirement:
 *      8 districts are shown below
 *      Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   are to be converted into UPPERCASE.
 *
 *Entity:
 *     ConvertToUpperCase
 *
 *Function Declaration:
 *     public static void main(String[] args)
 *
 *Jobs to be done:
 *     1)Create a ArrayList.
 *     2)Add the cities given as elements to the list.
 *     3)Iterate using lambda expression and covert city names to uppercase
 *     4)Print the list.
 *     
 *Pseudo code:
 *  
 *  public class ToUpperCaseDemo {
 *   
 *      public static void main(String[] args) {
 *      
 *      	List<String> list = new ArrayList<>();
            //add the elements 
            list.replaceAll(s -> s.toUpperCase());
            System.out.println(list)
 *      }
 * }      
 */
package com.training.java.exercise3.list;

import java.util.ArrayList;
import java.util.List;

public class ConvertCityNamesToUpperCase {
	
public static void main(String[] args) {
    	
        List<String> list = new ArrayList<>();
        list.add("Madurai");
        list.add("Coimbatore");
        list.add("Theni");
        list.add("Chennai");
        list.add("Karur");
        list.add("Salem");
        list.add("Erode");
        list.add("Trichy");
        
        list.replaceAll(city -> city.toUpperCase());
        System.out.println("After converting to uppercase:" + list);
    }

}
