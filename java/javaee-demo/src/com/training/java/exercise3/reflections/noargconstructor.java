/*
 Requirements:
   - Private Constructor with main class. 
 Entity:
   - Private Constructor
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1. Create an example for a No-Arg Constructor.
   2. Display the output.
*/
package com.training.java.exercise3.reflections;
class Main {

   int i;

   // constructor with no parameter
   private Main(){
       i = 5;
       System.out.println("Object created and i = " + i);
   }

   public static void main(String[] args) {

       // calling the constructor without any parameter
       Main obj = new Main();
   }
}