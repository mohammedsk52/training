/* 
Requirement:
    To find the maximum element of the given program using generic method.

Entity:
    public class MaximalElement

Function Declaration:
    public static void main(String[] args)
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    
Jobs to be Done:
    1. Declare a method public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    2. Compare the elements and find the maximum between the range and return it
    3. Under the main method create an object list or List<Integer>
    4. Call the method max() and print the result. 
*/

package com.training.java.exercise3.generics;

import java.util.Arrays;
import java.util.List;

public class  MaximalElement {

	public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
		T maximumElement = list.get(begin);
		for (++begin; begin < end; ++begin) {
			if (maximumElement.compareTo(list.get(begin)) < 0) {
				maximumElement = list.get(begin);
			}
		}   
		return maximumElement;
	}

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(11, 22, 33, 44, 55, 66, 77, 88);
		System.out.println(max(list, 0, list.size()));
	}
}