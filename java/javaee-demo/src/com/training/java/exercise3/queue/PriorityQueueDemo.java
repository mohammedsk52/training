/*Requirement:Create a queue using generic type and in both implementation Priority Queue
                  -> add atleast 5 elements
                  -> remove the front element
                  -> search a element in stack using contains 
                     key word and print boolean value value
                  -> print the size of stack
                  -> print the elements using Stream 
 * Entity:PriorityQueueDemo
 * Function Declaration:public static void main(String[] args)
 * Jobs To Be Done:1.consider the program
 *                 2.A priority queue is created  named queue
 *                 3.The elements to the queue are added using add method
 *                 4.The queue is printed
 *                 5.Then the first element is removed using remove method
 *                 6.the removed element is printed
 *                 7.true is printed is the particular element is present in queue
 *                 8.otherwise false will printed
 *                 9.size of the queue is printed
 *                 10.elements of the queue is printed using stream               
 */
package com.training.java.exercise3.queue;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class PriorityQueueDemo {
	
	public static void main(String[] args) {
		
		 Queue<Integer> queue = new PriorityQueue<>();
		 
		 //adding elements to queue
		 for (int number = 0; number < 5; number++) {
	            queue.add(number); 
	        }
		  System.out.println("Elements of queue "+ queue); 
		  
		  //remove first element
	      int removedElement = queue.remove(); 
	      System.out.println("removed element-" + removedElement); 
	        
	      //printing the boolean value for contains
	      System.out.println(queue.contains(1)); 
	      
	      //printing the size of the queue
	      System.out.println("Size of queue:" + queue.size()); 
	      
	      //printing the elements using stream
	      Stream<Integer> stream = queue.stream();
	      stream.forEach((number) -> System.out.println(number));
	}
}
