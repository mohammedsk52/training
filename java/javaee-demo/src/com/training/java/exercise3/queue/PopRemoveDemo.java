/*Requirement:
 * 	the difference between poll() and remove() method of queue interface
 * 
 * Entity:
 * 	PopRemoveDemo
 * 
 * Function Declaration:
 * 	public static void main(String[] args){}
 * 
 * Jobs To Be Done:
 * 	1)create a linkedList queue .
 *  2)use poll method 
 *  3)use remove method
 *    3.1)if queue is empty throw exception that queue is empty.
 *  
 * pseudo code:
 * 	public class PopRemoveDemo {

	public static void main(String[] args) {

			Queue<String> queue = new LinkedList<>();
			System.out.pritln(queue.poll());
			try {
			    System.out.println(queue.remove());
		    } catch(Exception exception) {
			    exception will be handled
		}
	}	
}
* Difference:Both poll() and remove() removes the first element in the queue.
*            If the queue is empty,
*               1)poll() will return null
*               2)remove() will throw NoSuchElementException
*/
package com.training.java.exercise3.queue;

import java.util.LinkedList;
import java.util.Queue;

public class PopRemoveDemo {

	public static void main(String[] args) {

		Queue<String> queue = new LinkedList<>();
		System.out.println("Element polled:" + queue.poll());
		
		try {
			System.out.println("Removed Element:" +queue.remove());
		} catch(Exception exception) {
			System.out.println("Queue is empty");
		}
	}	
}


