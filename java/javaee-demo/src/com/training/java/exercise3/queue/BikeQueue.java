/*Requirement:Consider a following code snippet
                Queue bike = new PriorityQueue();    
                bike.poll();
                System.out.println(bike.peek());    

           what will be output and complete the code.
 * Entity:BikeQueue
 * Function Declaration:public static void main(String[] args)
 * Jobs To Be Done:1.consider the program
 *                 2.A priorityQueue is created named bike
 *                 3.The first element is removed using pop
 *                 4.Then the peek element is printed
 *  //output:null
 */
package com.training.java.exercise3.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class BikeQueue {

	public static void main(String[] args) {
	
	Queue bike = new PriorityQueue();    
    bike.poll();
    System.out.println(bike.peek());    
}


}
